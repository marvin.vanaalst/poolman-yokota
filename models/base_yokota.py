from .shared_rate_functions import constant, michaelis_menten, ping_pong_bi_bi
from modelbase.ode import Model

# | Enzyme                           | Reaction                                                                                             | Keq    | Source       |
# | -------------------------------- | ------------------------------------------------------------                                         | ------ | ------------ |
# | phosphoglycolate phosphatase     | H2O + 2-Phosphoglycolate <=> Orthophosphate + Glycolate                                              | 3.1e5  | Equilibrator |
# | glycolate oxidase                | Oxygen + Glycolate <=> Hydrogen peroxide + Glyoxylate                                                | 3e15   | Equilibrator |
# | glycine transaminase             | L-Glutamate + Glyoxylate <=> 2-Oxoglutarate + Glycine                                                | 3e1    | Equilibrator |
# | glycine decarboxylase            | 2 Glycine(aq) + NAD (aq) + 2 H2O(l) ⇌ Serine(aq) + NH3(aq) + NADH(aq) + CO2(total)                   | 2.4e-4 | Equilibrator |
# | serine glyoxylate transaminase   | Glyoxylate + L-Serine <=> Glycine + Hydroxypyruvate                                                  | 6      | Equilibrator |
# | glycerate dehydrogenase          | NADH + Hydroxypyruvate <=> NAD  + D-Glycerate                                                        | 8.7e4  | Equilibrator |
# | glycerate kinase                 | ATP + D-Glycerate <=> ADP + 3-Phospho-D-glycerate                                                    | 4.9e2  | Equilibrator |
# | catalase                         | 2 Hydrogen peroxide <=> 2 H2O + Oxygen                                                               | 4.3e33 | Equilibrator |
# |                                  | Glyoxylate + H2O2 <=> Formate + CO2(total)                                                           | 7.1e65 | Equilibrator |
# |                                  | Hydroxypyruvate + H2O2 <=> Glycolate + CO2(total)                                                    | 3.3e72 | Equilibrator |

# Single steps of glycine cleavage system
# | glycine dehydrogenase            | NAD  + Glycine + Tetrahydrofolate + H2O <=> NADH + NH3 + 5,10-Methylenetetrahydrofolate + CO2(total) | 3.6e-3 | Equilibrator |
# | glycine hydroxymethyltransferase | H2O + Glycine + 5,10-Methylenetetrahydrofolate <=> L-Serine + Tetrahydrofolate                       | 0.07   | Equilibrator |


# kcat
# glyxolyate oxidase: 13 [1/s] | Arabidopsis thaliana | Kegg

cf = 0.145  # conversion factor 1mM = 0.145 micromoles per mg Chl


def add_phosphoglycolate_phosphatase(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "v_influx": 60.0 / cf,  # [mM/H]
        }
    )
    model.add_reaction(
        rate_name=f"phosphoglycolate_phosphatase{c_chl}{tissue}",  # R0
        function=constant,
        stoichiometry={f"GLYC{c_chl}{tissue}": 1},
        parameters=["v_influx"],
    )


def add_glycolate_oxidase(model: Model, c_chl: str, c_per: str, tissue: str) -> None:
    model.add_parameters(
        {
            "vmax_glycolate_oxidase": 100.0 / cf,  # [mM/H]
            "kms_glycolate_oxidase": 0.06,  # [mM]
        }
    )
    model.add_reaction(
        rate_name=f"glycolate_oxidase{c_per}{tissue}",  # R1
        function=michaelis_menten,
        stoichiometry={
            f"GLYC{c_chl}{tissue}": -1,
            f"GLYX{c_per}{tissue}": 1,
            f"H2O2{c_per}{tissue}": 1,
        },  # o2 is ignored
        parameters=[
            "vmax_glycolate_oxidase",
            "kms_glycolate_oxidase",
        ],
    )


def add_glycine_transaminase(model: Model, c_per: str, tissue: str) -> None:
    model.add_parameters(
        {
            "vmax_glycine_transaminase": 143.0 / cf,  # [mM/H]
            "kms_glycine_transaminase": 3.0,  # [mM]
        }
    )
    model.add_reaction(
        rate_name=f"glycine_transaminase{c_per}{tissue}",  # R2
        function=michaelis_menten,
        stoichiometry={
            f"GLYX{c_per}{tissue}": -1,
            f"GLY{c_per}{tissue}": 1,
        },  # glutamate & ketoglutarate are ignored
        parameters=[
            "vmax_glycine_transaminase",
            "kms_glycine_transaminase",
        ],
    )


def add_glycine_decarboxylase(model: Model, c_per: str, c_mit: str, tissue: str) -> None:
    model.add_parameters(
        {
            "vmax_glycine_decarboxylase": 100.0 / cf,  # [mM/H]
            "kms_glycine_decarboxylase": 6.0,  # [mM]
        }
    )
    model.add_reaction(
        rate_name=f"glycine_decarboxylase{c_mit}{tissue}",  # R3
        function=michaelis_menten,
        stoichiometry={
            f"GLY{c_per}{tissue}": -1,
            f"SER{c_per}{tissue}": 0.5,
        },  # NAD(H), co2 & nh4 are ignored
        parameters=[
            "vmax_glycine_decarboxylase",
            "kms_glycine_decarboxylase",
        ],
    )


def add_serine_glyoxylate_transaminase(model: Model, c_per: str, tissue: str) -> None:
    model.add_parameters(
        {
            "vmax_ser_gly_transaminase": 159.0 / cf,  # [mM/H]
            "kms_transaminase_serine": 2.72,  # [mM]
            "kms_transaminase_glyxoylate": 0.15,  # [mM]
        }
    )
    model.add_reaction(
        rate_name=f"serine_glyoxylate_transaminase{c_per}{tissue}",  # R4
        function=ping_pong_bi_bi,
        stoichiometry={
            f"GLYX{c_per}{tissue}": -1,
            f"SER{c_per}{tissue}": -1,
            f"GLY{c_per}{tissue}": 1,
            f"HPA{c_per}{tissue}": 1,
        },
        parameters=[
            "vmax_ser_gly_transaminase",
            "kms_transaminase_glyxoylate",
            "kms_transaminase_serine",
        ],
    )


def add_glycerate_dehydrogenase(model: Model, c_per: str, tissue: str) -> None:
    model.add_parameters(
        {
            "vmax_glycerate_dehydrogenase": 398.0 / cf,  # [mM/H]
            "kms_glycerate_dehydrogenase": 0.12,  # [mM]
        }
    )
    model.add_reaction(
        rate_name=f"glycerate_dehydrogenase{c_per}{tissue}",  # R5
        function=michaelis_menten,
        stoichiometry={f"HPA{c_per}{tissue}": -1},  # NAD(H) & GLYA is ignored
        parameters=[
            "vmax_glycerate_dehydrogenase",
            "kms_glycerate_dehydrogenase",
        ],
    )


def add_catalase(model: Model, c_per: str, tissue: str) -> None:
    model.add_parameters(
        {
            "vmax_catalase": 760500 / cf,  # [mM/H]
            "kms_catalase": 137.9,  # [mM]
        }
    )
    model.add_reaction(
        rate_name=f"catalase{c_per}{tissue}",  # R6
        function=michaelis_menten,
        stoichiometry={f"H2O2{c_per}{tissue}": -1},  # H2O & O2 is ignored
        parameters=["vmax_catalase", "kms_catalase"],
    )


def get_yokota(
    c_chl: str,  # chloroplast
    c_per: str,  # peroxisome
    c_mit: str,  # mitochondrium
    tissue: str,  # mesophyll / bundle sheath
) -> Model:
    model = Model()
    model.add_compounds(
        [
            f"GLYC{c_chl}{tissue}",  # glycolate
            f"GLYX{c_per}{tissue}",  # glyoxylate
            f"GLY{c_per}{tissue}",  # glycine
            f"SER{c_per}{tissue}",  # serine
            f"HPA{c_per}{tissue}",  # hydroxypyruvate
            f"H2O2{c_per}{tissue}",  # hydrogen peroxide
        ]
    )
    add_phosphoglycolate_phosphatase(model=model, c_chl=c_chl, tissue=tissue)
    add_glycolate_oxidase(model=model, c_chl=c_chl, c_per=c_per, tissue=tissue)
    add_glycine_transaminase(model=model, c_per=c_per, tissue=tissue)
    add_glycine_decarboxylase(model=model, c_per=c_per, c_mit=c_mit, tissue=tissue)
    add_serine_glyoxylate_transaminase(model=model, c_per=c_per, tissue=tissue)
    add_glycerate_dehydrogenase(model=model, c_per=c_per, tissue=tissue)
    add_catalase(model=model, c_per=c_per, tissue=tissue)
    return model


def add_yokota(model: Model, c_chl: str, c_per: str, c_mit: str, tissue: str) -> Model:
    return model + get_yokota(c_chl=c_chl, c_per=c_per, c_mit=c_mit, tissue=tissue)
