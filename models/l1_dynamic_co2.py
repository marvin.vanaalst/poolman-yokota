from .shared_rate_functions import diffusion, reversible_mass_action_keq_1s_1p
from .utils import filter_element, rename_element
from modelbase.ode import Model


def add_dynamic_co2(model: Model, c_chl: str, tissue: str) -> Model:
    model.remove_parameter("CO2")
    model.add_compounds(
        [
            f"CO2{c_chl}{tissue}",
            f"HCO3{c_chl}{tissue}",
        ]
    )
    model.add_parameters(
        {
            "CO2_ext": 407.8 / 1000,  # mM
            "k_diffusion_co2": 2.3,  # [1/s]; set this by hand so that co2 influx roughly matches previous rubisco flux
            "kf_hydration": 100,
            "keq_co2_hydration": 50,  # hco3:co2 is ~50:1 according to Straßburger
        }
    )
    model.add_reaction(
        f"CO2_influx{c_chl}{tissue}",
        function=diffusion,
        stoichiometry={f"CO2{c_chl}{tissue}": 1},
        parameters=["CO2_ext", "k_diffusion_co2"],
        reversible=True,
    )
    model.add_reaction(
        f"CO2_hydration{c_chl}{tissue}",
        function=reversible_mass_action_keq_1s_1p,
        stoichiometry={f"CO2{c_chl}{tissue}": -1, f"HCO3{c_chl}{tissue}": 1},
        parameters=["kf_hydration", "keq_co2_hydration"],
        reversible=True,
    )
    return model


def update_glycine_decarboxylase_dynamic_co2(
    model: Model, c_chl: str, c_per: str, c_mit: str, tissue: str
) -> Model:
    rate_name = f"glycine_decarboxylase{c_mit}{tissue}"
    rate = model.rates[rate_name]
    model.update_reaction(
        rate_name=rate_name,
        stoichiometry=model.stoichiometries[rate_name]
        | {
            f"CO2{c_chl}{tissue}": 0.5,
        },
        parameters=filter_element(rate.parameters, "CO2"),
        args=rename_element(rate.args, "CO2", f"CO2{c_chl}{tissue}"),
    )
    return model
