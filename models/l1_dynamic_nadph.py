from .shared_algebraic_functions import moiety_1
from .shared_rate_functions import (
    michaelis_menten,
    rapid_equilibrium_3s_3p,
    reversible_michaelis_menten_2s_3p,
)
from modelbase.ode import Model


def add_nadph_production(
    model: Model,
    c_chl: str,
    tissue: str,
) -> Model:
    """This module is for adding dynamic NADPH production
    in the case where the light reactions are not included"""
    model.remove_parameters(["NADPH", "NADP"])
    model.add_parameters(
        {
            "Vnadph": 2.816,
            "Kmnadph": 0.19,
            "NADPtot": 0.8,  # estimate from ~ 0.8 mM, Heineke1991
        }
    )
    model.add_compound(f"NADPH{c_chl}{tissue}")
    model.add_algebraic_module(
        module_name=f"nadp{c_chl}{tissue}",
        function=moiety_1,
        derived_compounds=[f"NADP{c_chl}{tissue}"],
        compounds=[f"NADPH{c_chl}{tissue}"],
        parameters=["NADPtot"],
    )
    model.add_reaction(
        rate_name=f"vnadph{c_chl}{tissue}",
        function=michaelis_menten,
        stoichiometry={f"NADPH{c_chl}{tissue}": 1},
        modifiers=[f"NADP{c_chl}{tissue}"],
        parameters=["Vnadph", "Kmnadph"],
    )
    return model


def update_cbb_reactions_dynamic_nadph(
    model: Model,
    c_chl: str,
    tissue: str,
) -> Model:
    model.update_reaction(
        rate_name=f"gadph{c_chl}{tissue}",
        function=rapid_equilibrium_3s_3p,
        stoichiometry={
            f"BPGA{c_chl}{tissue}": -1,
            f"NADPH{c_chl}{tissue}": -1,
            f"GAP{c_chl}{tissue}": 1,
        },
        modifiers=[
            f"Pi{c_chl}{tissue}",
            f"NADP{c_chl}{tissue}",
        ],
        parameters=["k_rapid_eq", "proton_pool_stroma", "q3"],
        args=[
            f"BPGA{c_chl}{tissue}",
            f"NADPH{c_chl}{tissue}",
            "proton_pool_stroma",
            f"GAP{c_chl}{tissue}",
            f"NADP{c_chl}{tissue}",
            f"Pi{c_chl}{tissue}",
            "k_rapid_eq",
            "q3",
        ],
        reversible=True,
    )
    return model


def update_yokota_reactions_dynamic_nadph(
    model: Model,
    c_chl: str,
    c_per: str,
    c_mit: str,
    tissue: str,
) -> Model:
    model.add_parameters(
        {
            "keq_glycine_decarboxylase": 1,
            "kmp_glycine_decarboxylase": 1,
        }
    )
    model.update_reaction(
        rate_name=f"glycine_decarboxylase{c_mit}{tissue}",  # R3
        function=reversible_michaelis_menten_2s_3p,
        stoichiometry={
            f"GLY{c_per}{tissue}": -2,
            # "NADP": -1,  # should be NAD
            # "H2O": -1,
            f"SER{c_per}{tissue}": 1,
            # f"CO2{c_chl}{tissue}": 1,
            # "NH4": 1,
            f"NADPH{c_chl}{tissue}": 1,  # should be NADH
        },
        modifiers=[f"NADP{c_chl}{tissue}"],
        parameters=[
            "vmax_glycine_decarboxylase",
            "kms_glycine_decarboxylase",
            "kmp_glycine_decarboxylase",
            "keq_glycine_decarboxylase",
            "CO2",
        ],
        args=[
            f"GLY{c_per}{tissue}",
            f"NADP{c_chl}{tissue}",
            f"SER{c_per}{tissue}",
            "CO2",
            f"NADPH{c_chl}{tissue}",
            "vmax_glycine_decarboxylase",
            "kms_glycine_decarboxylase",
            "kmp_glycine_decarboxylase",
            "keq_glycine_decarboxylase",
        ],
        reversible=True,
    )
    return model
