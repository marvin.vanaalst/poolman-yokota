from .shared_algebraic_functions import P_i, moiety_1, translocator_N
from .shared_rate_functions import (
    rapid_equilibrium_1s_1p,
    rapid_equilibrium_2s_1p,
    rapid_equilibrium_2s_2p,
    v3,
    v6,
    v9,
    v13,
    v16,
    v_out,
    vStarchProduction,
)
from modelbase.ode import Model

parameters = {
    "Vmax_rubisco_co2": 0.34 * 8,  # [mM/s], Pettersson 1988
    "Vmax_6": 0.2 * 8,  # [mM/s], Pettersson 1988
    "Vmax_9": 0.04 * 8,  # [mM/s], Pettersson 1988
    "Vmax_13": 0.9999 * 8,  # [mM/s], Pettersson 1988
    "Vmax_16": 2.8,  # [mM/s], Pettersson 1988
    "Vmax_starch": 0.04 * 8,  # [mM/s], Pettersson 1988
    "Vmax_efflux": 0.25 * 8,  # [mM/s], Pettersson 1988
    "kms_rubisco_rubp": 0.02,  # [mM], Pettersson 1988
    "kms_6": 0.03,  # [mM], Pettersson 1988
    "kms_9": 0.013,  # [mM], Pettersson 1988
    "kms_13_1": 0.05,  # [mM], Pettersson 1988
    "kms_13_2": 0.05,  # [mM], Pettersson 1988
    "kms_16_1": 0.014,  # [mM], Pettersson 1988
    "kms_16_2": 0.3,  # [mM], Pettersson 1988
    "kms_starch_1": 0.08,  # [mM], Pettersson 1988
    "kms_starch_2": 0.08,  # [mM], Pettersson 1988
    "K_pga": 0.25,  # [mM], Pettersson 1988
    "K_gap": 0.075,  # [mM], Pettersson 1988
    "K_dhap": 0.077,  # [mM], Pettersson 1988
    "K_pi": 0.63,  # [mM], Pettersson 1988
    "K_pxt": 0.74,  # [mM], Pettersson 1988
    "Ki_1_1": 0.04,  # [mM], Pettersson 1988
    "Ki_1_2": 0.04,  # [mM], Pettersson 1988
    "Ki_1_3": 0.075,  # [mM], Pettersson 1988
    "Ki_1_4": 0.9,  # [mM], Pettersson 1988
    "Ki_1_5": 0.07,  # [mM], Pettersson 1988
    "Ki_6_1": 0.7,  # [mM], Pettersson 1988
    "Ki_6_2": 12.0,  # [mM], Pettersson 1988
    "Ki_9": 12.0,  # [mM], Pettersson 1988
    "Ki_13_1": 2.0,  # [mM], Pettersson 1988
    "Ki_13_2": 0.7,  # [mM], Pettersson 1988
    "Ki_13_3": 4.0,  # [mM], Pettersson 1988
    "Ki_13_4": 2.5,  # [mM], Pettersson 1988
    "Ki_13_5": 0.4,  # [mM], Pettersson 1988
    "Ki_starch": 10.0,  # [mM], Pettersson 1988
    "Ka_starch_1": 0.1,  # [mM], Pettersson 1988
    "Ka_starch_2": 0.02,  # [mM], Pettersson 1988
    "Ka_starch_3": 0.02,  # [mM], Pettersson 1988
    "k_rapid_eq": 8e8,  # Rapid Equilibrium speed
    "q2": 0.00031,  # [], Pettersson 1988
    "q3": 16000000.0,  # [], Pettersson 1988
    "q4": 22.0,  # [], Pettersson 1988
    "q5": 7.1,  # [1/mM]], Pettersson 1988
    "q7": 0.084,  # [], Pettersson 1988
    "q8": 13.0,  # [1/mM]], Pettersson 1988
    "q10": 0.85,  # [], Pettersson 1988
    "q11": 0.4,  # [], Pettersson 1988
    "q12": 0.67,  # [], Pettersson 1988
    "q14": 2.3,  # [], Pettersson 1988
    "q15": 0.058,  # [], Pettersson 1988
    "Phosphate_total": 15.0,  # [mM], Pettersson 1988
    "AP_total": 0.5,  # [mM], Pettersson 1988
    "N_total": 0.5,  # [mM], Pettersson 1988
    "Pi_ext": 0.5,  # [mM], Pettersson 1988
    "pH_medium": 7.6,  # [], Pettersson 1988
    "pH_stroma": 7.9,  # [], Pettersson 1988
    "proton_pool_stroma": 1.2589254117941661e-05,  # [mM], Pettersson 1988
    "NADPH": 0.21,  # [mM], Pettersson 1988
    "NADP": 0.29,  # [mM], Pettersson 1988
    # NOT IN THE ORIGINAL MODEL
    "CO2": 1,
    "O2": 11.5,  # Fitted to get similar behaviour
    # rubisco parameters (Flamholz 2019)
    "Vmax_rubisco_o2": 0.87,  # [mM/s]
    "kms_rubisco_co2": 10 / 1000,  # [mM]
    "kms_rubisco_o2": 250 / 1000,  # [mM]
}


def _add_algebraic_modules(model: Model, c_chl: str, tissue: str) -> Model:
    model.add_algebraic_module(
        module_name=f"ADP_moiety{c_chl}{tissue}",
        function=moiety_1,
        derived_compounds=[f"ADP{c_chl}{tissue}"],
        compounds=[f"ATP{c_chl}{tissue}"],
        parameters=["AP_total"],
    )

    model.add_algebraic_module(
        module_name=f"Phosphate_moiety{c_chl}{tissue}",
        function=P_i,
        derived_compounds=[f"Pi{c_chl}{tissue}"],
        compounds=[
            f"PGA{c_chl}{tissue}",
            f"BPGA{c_chl}{tissue}",
            f"GAP{c_chl}{tissue}",
            f"DHAP{c_chl}{tissue}",
            f"FBP{c_chl}{tissue}",
            f"F6P{c_chl}{tissue}",
            f"G6P{c_chl}{tissue}",
            f"G1P{c_chl}{tissue}",
            f"SBP{c_chl}{tissue}",
            f"S7P{c_chl}{tissue}",
            f"E4P{c_chl}{tissue}",
            f"X5P{c_chl}{tissue}",
            f"R5P{c_chl}{tissue}",
            f"RUBP{c_chl}{tissue}",
            f"RU5P{c_chl}{tissue}",
            f"ATP{c_chl}{tissue}",
        ],
        parameters=["Phosphate_total"],
    )

    model.add_algebraic_module(
        module_name=f"translocator_N{c_chl}{tissue}",
        function=translocator_N,
        derived_compounds=[f"translocator_N{c_chl}{tissue}"],
        compounds=[
            f"Pi{c_chl}{tissue}",
            f"PGA{c_chl}{tissue}",
            f"GAP{c_chl}{tissue}",
            f"DHAP{c_chl}{tissue}",
        ],
        parameters=[
            "K_pxt",
            "Pi_ext",
            "K_pi",
            "K_pga",
            "K_gap",
            "K_dhap",
        ],
    )
    return model


def _add_rates(model: Model, c_chl: str, tissue: str) -> Model:
    model.add_reaction(
        rate_name=f"pgk{c_chl}{tissue}",  # v2, phosphoglycerate_kinase
        function=rapid_equilibrium_2s_2p,
        stoichiometry={
            f"PGA{c_chl}{tissue}": -1,
            f"ATP{c_chl}{tissue}": -1,
            f"BPGA{c_chl}{tissue}": 1,
        },
        modifiers=[f"ADP{c_chl}{tissue}"],
        parameters=["k_rapid_eq", "q2"],
        reversible=True,
    )
    model.add_reaction(
        rate_name=f"gadph{c_chl}{tissue}",  # v3, glyceraldehyde_phosphate_dehydrogenase
        function=v3,
        stoichiometry={
            f"BPGA{c_chl}{tissue}": -1,
            f"GAP{c_chl}{tissue}": 1,
        },
        modifiers=[f"Pi{c_chl}{tissue}"],
        parameters=[
            "proton_pool_stroma",
            "NADPH",
            "NADP",
            "k_rapid_eq",
            "q3",
        ],
        reversible=True,
    )
    model.add_reaction(
        rate_name=f"tpi{c_chl}{tissue}",  # triose-phosphate isomerase
        function=rapid_equilibrium_1s_1p,
        stoichiometry={
            f"GAP{c_chl}{tissue}": -1,
            f"DHAP{c_chl}{tissue}": 1,
        },
        parameters=["k_rapid_eq", "q4"],
        reversible=True,
    )
    model.add_reaction(
        rate_name=f"aldolase_1{c_chl}{tissue}",  # v5
        function=rapid_equilibrium_2s_1p,
        stoichiometry={
            f"GAP{c_chl}{tissue}": -1,
            f"DHAP{c_chl}{tissue}": -1,
            f"FBP{c_chl}{tissue}": 1,
        },
        parameters=["k_rapid_eq", "q5"],
        reversible=True,
    )
    model.add_reaction(
        rate_name=f"fbpase{c_chl}{tissue}",  # v6
        function=v6,
        stoichiometry={
            f"FBP{c_chl}{tissue}": -1,
            f"F6P{c_chl}{tissue}": 1,
        },
        modifiers=[
            f"F6P{c_chl}{tissue}",
            f"Pi{c_chl}{tissue}",
        ],
        parameters=["Vmax_6", "kms_6", "Ki_6_1", "Ki_6_2"],
    )
    model.add_reaction(
        rate_name=f"transketolase_1{c_chl}{tissue}",  # v7
        function=rapid_equilibrium_2s_2p,
        stoichiometry={
            f"GAP{c_chl}{tissue}": -1,
            f"F6P{c_chl}{tissue}": -1,
            f"E4P{c_chl}{tissue}": 1,
            f"X5P{c_chl}{tissue}": 1,
        },
        parameters=["k_rapid_eq", "q7"],
        reversible=True,
    )
    model.add_reaction(
        rate_name=f"aldolase_2{c_chl}{tissue}",  # v8
        function=rapid_equilibrium_2s_1p,
        stoichiometry={
            f"DHAP{c_chl}{tissue}": -1,
            f"E4P{c_chl}{tissue}": -1,
            f"SBP{c_chl}{tissue}": 1,
        },
        parameters=["k_rapid_eq", "q8"],
        reversible=True,
    )
    model.add_reaction(
        rate_name=f"sbpase{c_chl}{tissue}",  # v9
        function=v9,
        stoichiometry={
            f"SBP{c_chl}{tissue}": -1,
            f"S7P{c_chl}{tissue}": 1,
        },
        modifiers=[f"Pi{c_chl}{tissue}"],
        parameters=["Vmax_9", "kms_9", "Ki_9"],
    )
    model.add_reaction(
        rate_name=f"transketolase_2{c_chl}{tissue}",  # v10
        function=rapid_equilibrium_2s_2p,
        stoichiometry={
            f"GAP{c_chl}{tissue}": -1,
            f"S7P{c_chl}{tissue}": -1,
            f"X5P{c_chl}{tissue}": 1,
            f"R5P{c_chl}{tissue}": 1,
        },
        parameters=["k_rapid_eq", "q10"],
        reversible=True,
    )
    model.add_reaction(
        rate_name=f"rpi{c_chl}{tissue}",  # v11, ribose-5-phosphate isomerase
        function=rapid_equilibrium_1s_1p,
        stoichiometry={
            f"R5P{c_chl}{tissue}": -1,
            f"RU5P{c_chl}{tissue}": 1,
        },
        parameters=["k_rapid_eq", "q11"],
        reversible=True,
    )
    model.add_reaction(
        rate_name=f"rpe{c_chl}{tissue}",  # v12, ribulose-5-phosphate-3-epimerase
        function=rapid_equilibrium_1s_1p,
        stoichiometry={
            f"X5P{c_chl}{tissue}": -1,
            f"RU5P{c_chl}{tissue}": 1,
        },
        parameters=["k_rapid_eq", "q12"],
        reversible=True,
    )
    model.add_reaction(
        rate_name=f"prk{c_chl}{tissue}",  # v13, phosphoribulokinase
        function=v13,
        stoichiometry={
            f"RU5P{c_chl}{tissue}": -1,
            f"ATP{c_chl}{tissue}": -1,
            f"RUBP{c_chl}{tissue}": 1,
        },
        modifiers=[
            f"Pi{c_chl}{tissue}",
            f"PGA{c_chl}{tissue}",
            f"RUBP{c_chl}{tissue}",
            f"ADP{c_chl}{tissue}",
        ],
        parameters=[
            "Vmax_13",
            "kms_13_1",
            "kms_13_2",
            "Ki_13_1",
            "Ki_13_2",
            "Ki_13_3",
            "Ki_13_4",
            "Ki_13_5",
        ],
    )
    model.add_reaction(
        rate_name=f"gpi{c_chl}{tissue}",  # v15, Glucose-6-phosphate isomerase
        function=rapid_equilibrium_1s_1p,
        stoichiometry={
            f"F6P{c_chl}{tissue}": -1,
            f"G6P{c_chl}{tissue}": 1,
        },
        parameters=["k_rapid_eq", "q14"],
        reversible=True,
    )
    model.add_reaction(
        rate_name=f"pgm{c_chl}{tissue}",  # v15, phosphoglucomutase
        function=rapid_equilibrium_1s_1p,
        stoichiometry={
            f"G6P{c_chl}{tissue}": -1,
            f"G1P{c_chl}{tissue}": 1,
        },
        parameters=["k_rapid_eq", "q15"],
        reversible=True,
    )
    model.add_reaction(
        rate_name=f"atp_synthase{c_chl}{tissue}",  # v16
        function=v16,
        stoichiometry={f"ATP{c_chl}{tissue}": 1},
        modifiers=[
            f"ADP{c_chl}{tissue}",
            f"Pi{c_chl}{tissue}",
        ],
        parameters=["Vmax_16", "kms_16_1", "kms_16_2"],
    )
    model.add_reaction(
        rate_name=f"PGA_out{c_chl}{tissue}",
        function=v_out,
        stoichiometry={f"PGA{c_chl}{tissue}": -1},
        modifiers=[f"translocator_N{c_chl}{tissue}"],
        parameters=["Vmax_efflux", "K_pga"],
    )
    model.add_reaction(
        rate_name=f"GAP_out{c_chl}{tissue}",
        function=v_out,
        stoichiometry={f"GAP{c_chl}{tissue}": -1},
        modifiers=[f"translocator_N{c_chl}{tissue}"],
        parameters=["Vmax_efflux", "K_gap"],
    )
    model.add_reaction(
        rate_name=f"DHAP_out{c_chl}{tissue}",
        function=v_out,
        stoichiometry={f"DHAP{c_chl}{tissue}": -1},
        modifiers=[f"translocator_N{c_chl}{tissue}"],
        parameters=["Vmax_efflux", "K_dhap"],
    )
    model.add_reaction(
        rate_name=f"starch_production{c_chl}{tissue}",
        function=vStarchProduction,
        stoichiometry={
            f"G1P{c_chl}{tissue}": -1,
            f"ATP{c_chl}{tissue}": -1,
        },
        modifiers=[
            f"ADP{c_chl}{tissue}",
            f"Pi{c_chl}{tissue}",
            f"PGA{c_chl}{tissue}",
            f"F6P{c_chl}{tissue}",
            f"FBP{c_chl}{tissue}",
        ],
        parameters=[
            "Vmax_starch",
            "kms_starch_1",
            "kms_starch_2",
            "Ki_starch",
            "Ka_starch_1",
            "Ka_starch_2",
            "Ka_starch_3",
        ],
    )
    return model


def add_poolman(model: Model, c_chl: str, tissue: str) -> Model:
    """
    Parameters
    ----------
    tissue: compartment 1
    """
    model.add_compounds(
        [
            f"PGA{c_chl}{tissue}",
            f"BPGA{c_chl}{tissue}",
            f"GAP{c_chl}{tissue}",
            f"DHAP{c_chl}{tissue}",
            f"FBP{c_chl}{tissue}",
            f"F6P{c_chl}{tissue}",
            f"G6P{c_chl}{tissue}",
            f"G1P{c_chl}{tissue}",
            f"SBP{c_chl}{tissue}",
            f"S7P{c_chl}{tissue}",
            f"E4P{c_chl}{tissue}",
            f"X5P{c_chl}{tissue}",
            f"R5P{c_chl}{tissue}",
            f"RUBP{c_chl}{tissue}",
            f"RU5P{c_chl}{tissue}",
            f"ATP{c_chl}{tissue}",
        ]
    )
    model.add_parameters(parameters)
    model = _add_algebraic_modules(model, c_chl=c_chl, tissue=tissue)
    model = _add_rates(model, c_chl=c_chl, tissue=tissue)
    return model
