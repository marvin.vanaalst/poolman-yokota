from modelbase.ode import Model
from typing import Callable, Dict, Iterable, List


def modify_model(m: Model, *functions: Callable[[Model], Model]) -> Model:
    for func in functions:
        m = func(m)
    return m


def create_model(*functions: Callable[[Model], Model]) -> Model:
    m = Model()
    return modify_model(m, *functions)


def rename_element(
    it: List[str],
    old_name: str,
    new_name: str,
) -> List[str]:
    return [i if i != old_name else new_name for i in it]


def filter_element(it: List[str], element: str) -> List[str]:
    return [i for i in it if i != element]


def rename_cpds(
    cpds: Iterable[str],
    new_cpd_names: Dict[str, str],
) -> List[str]:
    return [new_cpd_names.get(i, i) for i in cpds]


def get_new_cpd_names(
    m: Model, compartment: str, all_compounds: bool = True
) -> Dict[str, str]:
    if all_compounds:
        return dict(
            zip(
                m.get_all_compounds(),
                [f"{i}_{compartment}" for i in m.get_all_compounds()],
            )
        )
    else:
        return dict(
            zip(
                m.get_compounds(),
                [f"{i}_{compartment}" for i in m.get_compounds()],
            )
        )


def add_compartment(
    m: Model,
    compartment: str,
) -> Model:
    cpds_to_remove = list(m.compounds)
    cpds_to_add = [f"{i}_{compartment}" for i in m.compounds]
    new_cpd_names = get_new_cpd_names(m, compartment)

    mods_to_remove = list(m.algebraic_modules.keys())
    rxns_to_remove = list(m.rates.keys())

    # Add new compounds
    m.add_compounds(cpds_to_add)

    # Add new algebraic modules
    for name in mods_to_remove:
        mod = m.algebraic_modules[name]
        m.add_algebraic_module(
            module_name=f"{name}_{compartment}",
            function=mod["function"],
            compounds=rename_cpds(mod["compounds"], new_cpd_names),
            derived_compounds=rename_cpds(mod["derived_compounds"], new_cpd_names),
            modifiers=rename_cpds(mod["modifiers"], new_cpd_names),
            parameters=mod["parameters"],
            dynamic_variables=rename_cpds(mod["dynamic_variables"], new_cpd_names),
            args=rename_cpds(mod["args"], new_cpd_names),
        )

    # Add new reactions
    for name in rxns_to_remove:
        rxn = m.rates[name]
        stoich = m.stoichiometries[name]
        m.add_reaction(
            rate_name=f"{name}_{compartment}",
            function=rxn["function"],
            stoichiometry=dict(
                zip(rename_cpds(stoich.keys(), new_cpd_names), stoich.values())
            ),
            modifiers=rename_cpds(rxn["modifiers"], new_cpd_names),
            parameters=rxn["parameters"],
            dynamic_variables=rename_cpds(rxn["dynamic_variables"], new_cpd_names),
            args=rename_cpds(rxn["args"], new_cpd_names),
            reversible=rxn["reversible"],
        )
    m.remove_reactions(rxns_to_remove)
    m.remove_algebraic_modules(mods_to_remove)
    m.remove_compounds(cpds_to_remove)
    return m


def rename_y0(
    y0: Dict[str, float],
    m: Model,
    compartment: str,
) -> Dict[str, float]:
    new_cpd_names = get_new_cpd_names(m, compartment, False)
    return {new_cpd_names[k]: v for k, v in y0.items()}


def multiply_parameter(model: Model, parameter: str, factor: float) -> Model:
    model.update_parameter(parameter, model.parameters[parameter] * factor)
    return model


def knockout_phosphoglycolate_phosphatase(model: Model) -> Model:
    model.remove_reaction("phosphoglycolate_phosphatase")
    model.remove_parameters(
        [
            "vmax_phosphoglycolate_phosphatase",
            "kms_phosphoglycolate_phosphatase",
        ]
    )
    return model


def knockout_glycine_decarboxylase(model: Model) -> Model:
    model.remove_reaction("glycine_decarboxylase")
    model.remove_parameters(["vmax_glycine_decarboxylase", "kms_glycine_decarboxylase"])
    return model


def knockout_glycolate_oxidase(model: Model) -> Model:
    model.remove_reaction(rate_name="glycolate_oxidase")
    model.remove_parameters(
        [
            "vmax_glycolate_oxidase",
            "kms_glycolate_oxidase",
        ]
    )
    return model


def knockout_glycine_transaminase(model: Model) -> Model:
    model.remove_reaction(rate_name="glycine_transaminase")
    model.remove_parameters(
        [
            "vmax_glycine_transaminase",
            "kms_glycine_transaminase",
        ]
    )
    return model


def knockout_serine_glyoxylate_transaminase(model: Model) -> Model:
    model.remove_reaction(rate_name="serine_glyoxylate_transaminase")
    model.remove_parameters(
        [
            "vmax_ser_gly_transaminase",
            "kms_transaminase_glyxoylate",
            "kms_transaminase_serine",
        ]
    )
    return model


def knockout_catalase(model: Model) -> Model:
    model.remove_reaction(rate_name="catalase")
    model.remove_parameters(
        [
            "vmax_catalase",
            "kms_catalase",
        ]
    )
    return model


def knockout_glycerate_dehydrogenase(model: Model) -> Model:
    model.remove_reaction(rate_name="glycerate_dehydrogenase")
    model.remove_parameters(
        [
            "vmax_glycerate_dehydrogenase",
            "kms_glycerate_dehydrogenase",
        ]
    )
    return model


def add_parameters_callback(parameters: Dict[str, float]) -> Callable[..., Model]:
    def inner(model: Model) -> Model:
        model.update_parameters(parameters)
        return model

    return inner


def remove_parameters(model: Model, parameters: List[str]) -> Model:
    model.remove_parameters(parameters)
    return model


def remove_parameters_callback(parameters: List[str]) -> Callable[..., Model]:
    def inner(model: Model) -> Model:
        model.remove_parameters(parameters)
        return model

    return inner


def remove_reaction_callback(reaction: str) -> Callable[..., Model]:
    def inner(model: Model) -> Model:
        model.remove_reaction(reaction)
        return model

    return inner


def remove_compounds_callback(compounds: List[str]) -> Callable[..., Model]:
    def remove_func(m: Model) -> Model:
        m.remove_compounds(compounds)
        return m

    return remove_func
