from .shared_rate_functions import ping_pong_2, ping_pong_3
from modelbase.ode import Model


def add_taco_bypass_imm(model: Model, c_chl: str, tissue: str) -> Model:
    """Add coa bypass with irreversible michaelis-menten kinetics

    | Enzyme                                                       | kcat [1/s] | Km [mM]    | Source          |
    |-----------------------------------                           |------------|---------   |--------         |
    | Glycolyl-CoA synthetase (glycolate)                          | 11.1       | 13         | Scheffen 2021   |
    | Glycolyl-CoA carboxylase (glycolyl-coa)                      | 5.6        | 0.15       | Scheffen 2021   |
    | Tartronyl-CoA reductase (tartronyl-coa)                      | 1.4        | 0.03       | Scheffen 2021   |
    | tartronate semialdehyde reductase (tartronate semialdehyde)  | 243        | 0.05 - 0.4 | brenda 1.1.1.60 |

    """
    model.add_compounds(
        [
            f"glycolyl_coa{c_chl}{tissue}",
            f"tartronyl_coa{c_chl}{tissue}",
            f"tartronate_semialdehyde{c_chl}{tissue}",
        ]
    )
    model.add_parameters(
        {
            "vmax_glycolyl_coa_synthetase": 11.1,
            "kms_glycolyl_coa_synthetase_glycolate": 13,
            "kms_glycolyl_coa_synthetase_atp": 1,
            "kms_glycolyl_coa_synthetase_pi": 1,
            "vmax_glycolyl_coa_carboxylase": 5.6,
            "kms_glycolyl_coa_carboxylase_glycoly_coa": 0.15,
            "kms_glycolyl_coa_carboxylase_hco3": 1,
            "kms_glycolyl_coa_carboxylase_atp": 1,
            "vmax_tartronyl_coa_reductase": 1.4,
            "kms_tartronyl_coa_reductase_tartronyl_coa": 0.03,
            "kms_tartronyl_coa_reductase_nadph": 1,
            "vmax_tartronate_semialdehyde_reductase": 243,
            "kms_tartronate_semialdehyde_reductase_tartronate_semi": 0.4,
            "kms_tartronate_semialdehyde_reductase_nadph": 1,
        }
    )
    model.add_reaction(
        rate_name=f"glycolyl_coa_synthetase{c_chl}{tissue}",
        function=ping_pong_3,
        stoichiometry={
            f"GLYC{c_chl}{tissue}": -1,
            f"ATP{c_chl}{tissue}": -1,
            # "Pi": -1,
            f"glycolyl_coa{c_chl}{tissue}": 1,
            # "AMP": 1,
            # "PPi": 1,
        },
        modifiers=[f"Pi{c_chl}{tissue}"],
        parameters=[
            "vmax_glycolyl_coa_synthetase",
            "kms_glycolyl_coa_synthetase_glycolate",
            "kms_glycolyl_coa_synthetase_atp",
            "kms_glycolyl_coa_synthetase_pi",
        ],
    )
    model.add_reaction(
        rate_name=f"glycolyl_coa_carboxylase{c_chl}{tissue}",
        function=ping_pong_3,
        stoichiometry={
            f"glycolyl_coa{c_chl}{tissue}": -1,
            f"HCO3{c_chl}{tissue}": -1,
            f"ATP{c_chl}{tissue}": -1,
            f"tartronyl_coa{c_chl}{tissue}": 1,
            # "ADP": 1,
        },
        parameters=[
            "vmax_glycolyl_coa_carboxylase",
            "kms_glycolyl_coa_carboxylase_glycoly_coa",
            "kms_glycolyl_coa_carboxylase_hco3",
            "kms_glycolyl_coa_carboxylase_atp",
        ],
    )
    model.add_reaction(
        rate_name=f"tartronyl_coa_reductase{c_chl}{tissue}",
        function=ping_pong_2,
        stoichiometry={
            f"tartronyl_coa{c_chl}{tissue}": -1,
            f"NADPH{c_chl}{tissue}": -1,
            f"tartronate_semialdehyde{c_chl}{tissue}": 1,
            # "NADP": 1,
        },
        # modifiers=[f"NADP{c_chl}{tissue}"],
        parameters=[
            "vmax_tartronyl_coa_reductase",
            "kms_tartronyl_coa_reductase_tartronyl_coa",
            "kms_tartronyl_coa_reductase_nadph",
        ],
    )
    model.add_reaction(
        rate_name=f"tartronate_semialdehyde_reductase{c_chl}{tissue}",
        function=ping_pong_2,
        stoichiometry={
            f"tartronate_semialdehyde{c_chl}{tissue}": -1,
            f"NADPH{c_chl}{tissue}": -1,
            f"GLYA{c_chl}{tissue}": 1,
            # "NADP": 1,
        },
        # modifiers=[f"NADP{c_chl}{tissue}"],
        parameters=[
            "vmax_tartronate_semialdehyde_reductase",
            "kms_tartronate_semialdehyde_reductase_tartronate_semi",
            "kms_tartronate_semialdehyde_reductase_nadph",
        ],
    )

    return model
