from typing import List


def moiety_1(concentration: float, total: float) -> float:
    return total - concentration


def moiety_2(x1: float, x2: float, total: float) -> float:
    return total - x1 - x2


def P_i(
    PGA: float,
    BPGA: float,
    GAP: float,
    DHAP: float,
    FBP: float,
    F6P: float,
    G6P: float,
    G1P: float,
    SBP: float,
    S7P: float,
    E4P: float,
    X5P: float,
    R5P: float,
    RUBP: float,
    RU5P: float,
    ATP: float,
    phosphate_total: float,
) -> List[float]:
    return [
        phosphate_total
        - (
            PGA
            + 2 * BPGA
            + GAP
            + DHAP
            + 2 * FBP
            + F6P
            + G6P
            + G1P
            + 2 * SBP
            + S7P
            + E4P
            + X5P
            + R5P
            + 2 * RUBP
            + RU5P
            + ATP
        )
    ]


def translocator_N(
    Pi: float,
    PGA: float,
    GAP: float,
    DHAP: float,
    Kpxt: float,
    Pext: float,
    Kpi: float,
    Kpga: float,
    Kgap: float,
    Kdhap: float,
) -> List[float]:
    return [
        (
            1
            + (1 + (Kpxt / Pext))
            * ((Pi / Kpi) + (PGA / Kpga) + (GAP / Kgap) + (DHAP / Kdhap))
        )
    ]
