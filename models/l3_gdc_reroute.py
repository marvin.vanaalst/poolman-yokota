from .utils import filter_element, rename_element
from modelbase.ode import Model


def add_gcd_reroute(
    model: Model,
    c_chl: str,
    c_per: str,
    c_mit: str,
    tissue1: str,
    tissue2: str,
) -> Model:
    rate_name = f"glycine_decarboxylase{c_mit}{tissue1}"
    rate = model.rates[rate_name]
    model.update_reaction(
        rate_name=rate_name,
        stoichiometry={
            f"GLY{c_per}{tissue1}": -2,
            f"NADPH{c_chl}{tissue1}": 1,  # should be NADH
            f"SER{c_per}{tissue1}": 1,
            f"CO2{c_chl}{tissue2}": 1,
        },
        args=rename_element(rate.args, f"CO2{c_chl}{tissue1}", f"CO2{c_chl}{tissue2}"),
    )
    return model
