import numpy as np
from typing import Tuple

###############################################################################
# Generic
###############################################################################


def constant(k: float) -> float:
    return k


def proportional(base: float, factor: float) -> float:
    return base * factor


def diffusion(
    x: float,
    y: float,
    k: float,
) -> float:
    return k * (y - x)


def michaelis_menten(
    s: float,
    vmax: float,
    km: float,
) -> float:
    return vmax * s / (km + s)


def mass_action_1s(
    s1: float,
    k_fwd: float,
) -> float:
    return k_fwd * s1


def mass_action_2s(
    s1: float,
    s2: float,
    k_fwd: float,
) -> float:
    return k_fwd * s1 * s2


def mass_action_3s(
    s1: float,
    s2: float,
    s3: float,
    k_fwd: float,
) -> float:
    return k_fwd * s1 * s2 * s3


def reversible_mass_action_keq_1s_1p(
    s1: float,
    p1: float,
    kf: float,
    keq: float,
) -> float:
    return kf * (s1 - p1 / keq)


def reversible_mass_action_keq_1s_2p(
    s1: float,
    p1: float,
    p2: float,
    kf: float,
    keq: float,
) -> float:
    return kf * (s1 - p1 * p2 / keq)


def reversible_mass_action_keq_1s_3p(
    s1: float,
    p1: float,
    p2: float,
    p3: float,
    kf: float,
    keq: float,
) -> float:
    return kf * (s1 - p1 * p2 * p3 / keq)


def reversible_mass_action_keq_2s_1p(
    s1: float,
    s2: float,
    p1: float,
    kf: float,
    keq: float,
) -> float:
    return kf * (s1 * s2 - p1 / keq)


def reversible_mass_action_keq_2s_2p(
    s1: float,
    s2: float,
    p1: float,
    p2: float,
    kf: float,
    keq: float,
) -> float:
    return kf * (s1 * s2 - p1 * p2 / keq)


def reversible_mass_action_keq_2s_3p(
    s1: float,
    s2: float,
    p1: float,
    p2: float,
    p3: float,
    kf: float,
    keq: float,
) -> float:
    return kf * (s1 * s2 - p1 * p2 * p3 / keq)


def reversible_mass_action_keq_3s_3p(
    s1: float,
    s2: float,
    s3: float,
    p1: float,
    p2: float,
    p3: float,
    kf: float,
    keq: float,
) -> float:
    return kf * (s1 * s2 * s3 - p1 * p2 * p3 / keq)


def rapid_equilibrium_1s_1p(
    s1: float,
    p1: float,
    kRE: float,
    q: float,
) -> float:
    return kRE * (s1 - p1 / q)


def rapid_equilibrium_2s_1p(
    s1: float,
    s2: float,
    p1: float,
    kRE: float,
    q: float,
) -> float:
    return kRE * (s1 * s2 - p1 / q)


def rapid_equilibrium_2s_2p(
    s1: float,
    s2: float,
    p1: float,
    p2: float,
    kRE: float,
    q: float,
) -> float:
    return kRE * (s1 * s2 - (p1 * p2) / q)


def rapid_equilibrium_3s_3p(
    s1: float,
    s2: float,
    s3: float,
    p1: float,
    p2: float,
    p3: float,
    kRE: float,
    q: float,
) -> float:
    return kRE * (s1 * s2 * s3 - (p1 * p2 * p3) / q)


def reversible_michaelis_menten_1s_1p(
    s1: float,
    p1: float,
    vmax: float,
    kms: float,
    kmp: float,
    keq: float,
) -> float:
    return (vmax / kms) * (1 / (1 + (s1) / kms + (p1) / kmp)) * (s1 - (p1) / keq)


def reversible_michaelis_menten_1s_2p(
    s1: float,
    p1: float,
    p2: float,
    vmax: float,
    kms: float,
    kmp: float,
    keq: float,
) -> float:
    return (
        (vmax / kms) * (1 / (1 + (s1) / kms + (p1 * p2) / kmp)) * (s1 - (p1 * p2) / keq)
    )


def reversible_michaelis_menten_2s_2p(
    s1: float,
    s2: float,
    p1: float,
    p2: float,
    vmax: float,
    kms: float,
    kmp: float,
    keq: float,
) -> float:
    return (
        (vmax / kms)
        * (1 / (1 + (s1 * s2) / kms + (p1 * p2) / kmp))
        * (s1 * s2 - (p1 * p2) / keq)
    )


def reversible_michaelis_menten_2s_3p(
    s1: float,
    s2: float,
    p1: float,
    p2: float,
    p3: float,
    vmax: float,
    kms: float,
    kmp: float,
    keq: float,
) -> float:
    return (
        (vmax / kms)
        * (1 / (1 + (s1 * s2) / kms + (p1 * p2 * p3) / kmp))
        * (s1 * s2 - (p1 * p2 * p3) / keq)
    )


def reversible_michaelis_menten_3s_3p(
    s1: float,
    s2: float,
    s3: float,
    p1: float,
    p2: float,
    p3: float,
    vmax: float,
    kms: float,
    kmp: float,
    keq: float,
) -> float:
    return (
        (vmax / kms)
        * (1 / (1 + (s1 * s2 * s3) / kms + (p1 * p2 * p3) / kmp))
        * (s1 * s2 * s3 - (p1 * p2 * p3) / keq)
    )


###############################################################################
# Custom
###############################################################################


def ping_pong_bi_bi(
    s1: float,
    s2: float,
    vmax: float,
    km_s1: float,
    km_s2: float,
) -> float:
    return (
        vmax * s1 * s2 / ((1 / (km_s1 * km_s2)) + (s1 / km_s1) + (s2 / km_s2) + s1 * s2)
    )


def ping_pong_2(
    A: float,
    B: float,
    vmax: float,
    kmA: float,
    kmB: float,
) -> float:
    return vmax * A * B / (A * B + kmA * B + kmB * A)


def ping_pong_3(
    A: float,
    B: float,
    C: float,
    vmax: float,
    kmA: float,
    kmB: float,
    kmC: float,
) -> float:
    return (vmax * A * B * C) / (
        A * B * C + (kmA * B * C) + (kmB * A * C) + (kmC * A * B)
    )


def ping_pong_4(
    A: float,
    B: float,
    C: float,
    D: float,
    vmax: float,
    kmA: float,
    kmB: float,
    kmC: float,
    kmD: float,
) -> float:
    return (vmax * A * B * C * D) / (
        A * B * C * D
        + (kmA * B * C * D)
        + (kmB * A * C * D)
        + (kmC * A * B * D)
        + (kmD * A * B * C)
    )


def v_out(
    s1: float,
    N_total: float,
    VMax_efflux: float,
    K_efflux: float,
) -> float:
    return (VMax_efflux * s1) / (N_total * K_efflux)


def v3(
    BPGA: float,
    GAP: float,
    Pi: float,
    proton_pool_stroma: float,
    NADPH: float,
    NADP: float,
    kRE: float,
    q3: float,
) -> float:
    return kRE * ((NADPH * BPGA * proton_pool_stroma) - (1 / q3) * (GAP * NADP * Pi))


def v6(
    FBP: float,
    F6P: float,
    P: float,
    V6: float,
    Km6: float,
    Ki61: float,
    Ki62: float,
) -> float:
    return (V6 * FBP) / (FBP + Km6 * (1 + (F6P / Ki61) + (P / Ki62)))


def v9(
    SBP: float,
    P: float,
    V9: float,
    Km9: float,
    Ki9: float,
) -> float:
    return (V9 * SBP) / (SBP + Km9 * (1 + (P / Ki9)))


def v13(
    RU5P: float,
    ATP: float,
    Pi: float,
    PGA: float,
    RUBP: float,
    ADP: float,
    V13: float,
    Km131: float,
    Km132: float,
    Ki131: float,
    Ki132: float,
    Ki133: float,
    Ki134: float,
    Ki135: float,
) -> float:
    return (V13 * RU5P * ATP) / (
        (RU5P + Km131 * (1 + (PGA / Ki131) + (RUBP / Ki132) + (Pi / Ki133)))
        * (ATP * (1 + (ADP / Ki134)) + Km132 * (1 + (ADP / Ki135)))
    )


def v16(
    ADP: float,
    Phosphate_i: float,
    V16: float,
    Km161: float,
    Km162: float,
) -> float:
    return (V16 * ADP * Phosphate_i) / ((ADP + Km161) * (Phosphate_i + Km162))


def vStarchProduction(
    G1P: float,
    ATP: float,
    ADP: float,
    Pi: float,
    PGA: float,
    F6P: float,
    FBP: float,
    Vst: float,
    Kmst1: float,
    Kmst2: float,
    Kist: float,
    Kast1: float,
    Kast2: float,
    Kast3: float,
) -> float:
    return (Vst * G1P * ATP) / (
        (G1P + Kmst1)
        * (
            (1 + (ADP / Kist)) * (ATP + Kmst2)
            + ((Kmst2 * Pi) / (Kast1 * PGA + Kast2 * F6P + Kast3 * FBP))
        )
    )


def glycerate_kinase(
    s1: float,
    s2: float,
    i1: float,
    vmax: float,
    km_s1: float,
    km_s2: float,
    ki1: float,
) -> float:
    # Ping-pong mechanism with inhibition for the second substrate
    return vmax * s1 * s2 / (s1 * s2 + s1 * km_s1 + s2 * km_s2 * (1 + i1 / ki1))


def keq_pq_red(
    E0_QA: float,
    F: float,
    E0_PQ: float,
    pHstroma: float,
    dG_pH: float,
    RT: float,
) -> float:
    DG1 = -E0_QA * F
    DG2 = -2 * E0_PQ * F
    DG = -2 * DG1 + DG2 + 2 * pHstroma * dG_pH
    K = np.exp(-DG / RT)
    return K  # type: ignore


def keq_cyc(
    E0_Fd: float,
    F: float,
    E0_PQ: float,
    pHstroma: float,
    dG_pH: float,
    RT: float,
) -> float:
    DG1 = -E0_Fd * F
    DG2 = -2 * E0_PQ * F
    DG = -2 * DG1 + DG2 + 2 * dG_pH * pHstroma
    K = np.exp(-DG / RT)
    return K  # type: ignore


def keq_faf_d(
    E0_FA: float,
    F: float,
    E0_Fd: float,
    RT: float,
) -> float:
    DG1 = -E0_FA * F
    DG2 = -E0_Fd * F
    DG = -DG1 + DG2
    K = np.exp(-DG / RT)
    return K  # type: ignore


def keq_pcp700(
    E0_PC: float,
    F: float,
    E0_P700: float,
    RT: float,
) -> float:
    DG1 = -E0_PC * F
    DG2 = -E0_P700 * F
    DG = -DG1 + DG2
    K = np.exp(-DG / RT)
    return K  # type: ignore


def keq_fnr(
    E0_Fd: float,
    F: float,
    E0_NADP: float,
    pHstroma: float,
    dG_pH: float,
    RT: float,
) -> float:
    DG1 = -E0_Fd * F
    DG2 = -2 * E0_NADP * F
    DG = -2 * DG1 + DG2 + dG_pH * pHstroma
    K = np.exp(-DG / RT)
    return K  # type: ignore


def keq_atp(
    pH: float,
    DeltaG0_ATP: float,
    dG_pH: float,
    HPR: float,
    pHstroma: float,
    Pi_mol: float,
    RT: float,
) -> float:
    DG = DeltaG0_ATP - dG_pH * HPR * (pHstroma - pH)
    Keq = Pi_mol * np.exp(-DG / RT)
    return Keq  # type: ignore


def keq_cytb6f(
    pH: float,
    F: float,
    E0_PQ: float,
    E0_PC: float,
    pHstroma: float,
    RT: float,
    dG_pH: float,
) -> float:
    DG1 = -2 * F * E0_PQ
    DG2 = -F * E0_PC
    DG = -(DG1 + 2 * dG_pH * pH) + 2 * DG2 + 2 * dG_pH * (pHstroma - pH)
    Keq = np.exp(-DG / RT)
    return Keq  # type: ignore


def ph_lumen(protons: float) -> float:
    return -np.log10(protons * 2.5e-4)  # type: ignore


def protons_stroma(ph: float) -> float:
    return 4e3 * 10 ** (-ph)


def ps2_crosssection(
    LHC: float,
    staticAntII: float,
    staticAntI: float,
) -> float:
    return staticAntII + (1 - staticAntII - staticAntI) * LHC


def quencher(
    Psbs: float,
    Vx: float,
    Psbsp: float,
    Zx: float,
    y0: float,
    y1: float,
    y2: float,
    y3: float,
    kZSat: float,
) -> float:
    """co-operative 4-state quenching mechanism
    gamma0: slow quenching of (Vx - protonation)
    gamma1: fast quenching (Vx + protonation)
    gamma2: fastest possible quenching (Zx + protonation)
    gamma3: slow quenching of Zx present (Zx - protonation)
    """
    ZAnt = Zx / (Zx + kZSat)
    return y0 * Vx * Psbs + y1 * Vx * Psbsp + y2 * ZAnt * Psbsp + y3 * ZAnt * Psbs


def ps2states(
    PQ: float,
    PQred: float,
    ps2cs: float,
    Q: float,
    PSIItot: float,
    k2: float,
    kF: float,
    _kH: float,
    Keq_PQred: float,
    kPQred: float,
    pfd: float,
    kH0: float,
) -> Tuple[float, float, float, float]:
    L = ps2cs * pfd
    kH = kH0 + _kH * Q
    k3p = kPQred * PQ
    k3m = kPQred * PQred / Keq_PQred
    Bs = []
    if isinstance(kH, float) and isinstance(PQ, np.ndarray):
        kH = np.repeat(kH, len(PQ))
    for L, kH, k3p, k3m in zip(L, kH, k3p, k3m):  # type: ignore
        M = np.array(
            [
                [-L - k3m, kH + kF, k3p, 0],
                [L, -(kH + kF + k2), 0, 0],
                [0, 0, L, -(kH + kF)],
                [1, 1, 1, 1],
            ]
        )
        A = np.array([0, 0, 0, PSIItot])
        B0, B1, B2, B3 = np.linalg.solve(M, A)
        Bs.append([B0, B1, B2, B3])
    return np.array(Bs).T  # type: ignore


def fluorescence(
    Q: float,
    B0: float,
    B2: float,
    ps2cs: float,
    k2: float,
    kF: float,
    kH: float,
) -> float:
    return (ps2cs * kF * B0) / (kF + k2 + kH * Q) + (ps2cs * kF * B2) / (kF + kH * Q)


def ps1states(
    PC: float,
    PCred: float,
    Fd: float,
    Fdred: float,
    ps2cs: float,
    PSItot: float,
    kFdred: float,
    Keq_FAFd: float,
    Keq_PCP700: float,
    kPCox: float,
    pfd: float,
) -> float:
    """
    QSSA calculates open state of PSI
    depends on reduction states of plastocyanin and ferredoxin
    C = [PC], F = [Fd] (ox. forms)
    """
    L = (1 - ps2cs) * pfd
    A1 = PSItot / (
        1
        + L / (kFdred * Fd)
        + (1 + Fdred / (Keq_FAFd * Fd))
        * (PC / (Keq_PCP700 * PCred) + L / (kPCox * PCred))
    )
    return A1


def ps1(A: float, ps2cs: float, pfd: float) -> float:
    return (1 - ps2cs) * pfd * A


def ps2(B1: float, k2: float) -> float:
    return 0.5 * k2 * B1


def ptox(
    pq_red: float,
    kPTOX: float,
    O2ext: float,
) -> float:
    """calculates reaction rate of PTOX"""
    return pq_red * kPTOX * O2ext


def b6f(
    PC: float,
    Pox: float,
    Pred: float,
    PCred: float,
    Keq_B6f: float,
    kCytb6f: float,
) -> float:
    return np.maximum(kCytb6f * (Pred * PC**2 - (Pox * PCred**2) / Keq_B6f), -kCytb6f)  # type: ignore


def cyclic_electron_flow(Pox: float, Fdred: float, kcyc: float) -> float:
    return kcyc * Fdred**2 * Pox


def fnr(
    Fd: float,
    Fdred: float,
    NADPH: float,
    NADP: float,
    KM_FNR_F: float,
    KM_FNR_N: float,
    EFNR: float,
    kcatFNR: float,
    Keq_FNR: float,
    convf: float,
) -> float:
    fdred = Fdred / KM_FNR_F
    fdox = Fd / KM_FNR_F
    nadph = (
        NADPH / convf
    ) / KM_FNR_N  # NADPH requires conversion to mmol/mol of chlorophyll
    nadp = (
        NADP / convf
    ) / KM_FNR_N  # NADP requires conversion to mmol/mol of chlorophyll
    return (
        EFNR
        * kcatFNR
        * ((fdred**2) * nadp - ((fdox**2) * nadph) / Keq_FNR)
        / (
            (1 + fdred + fdred**2) * (1 + nadp)
            + (1 + fdox + fdox**2) * (1 + nadph)
            - 1
        )
    )


def leak(
    protons_lumen: float,
    k_leak: float,
    ph_stroma: float,
) -> float:
    return k_leak * (protons_lumen - protons_stroma(ph_stroma))


def state_transition_ps1_ps2(
    Ant: float,
    Pox: float,
    kStt7: float,
    PQtot: float,
    KM_ST: float,
    n_ST: float,
) -> float:
    kKin = kStt7 * (1 / (1 + ((Pox / PQtot) / KM_ST) ** n_ST))
    return kKin * Ant


def atp_synthase(
    ATP: float,
    ADP: float,
    Keq_ATPsynthase: float,
    kATPsynth: float,
    convf: float,
) -> float:
    return kATPsynth * (ADP / convf - ATP / convf / Keq_ATPsynthase)


def protonation_hill(
    Vx: float,
    H: float,
    nH: float,
    k_fwd: float,
    kphSat: float,
) -> float:
    return k_fwd * ((H**nH) / (H**nH + protons_stroma(kphSat) ** nH)) * Vx
