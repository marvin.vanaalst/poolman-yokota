from .shared_algebraic_functions import moiety_2
from modelbase.ode import Model
from typing import Tuple


def ps1analytic_mehler(
    PC: float,
    PCred: float,
    Fd: float,
    Fdred: float,
    ps2cs: float,
    PSItot: float,
    kFdred: float,
    KeqF: float,
    KeqC: float,
    kPCox: float,
    pfd: float,
    k0: float,
    O2: float,
) -> Tuple[float, float, float]:
    """
    QSSA calculates open state of PSI
    depends on reduction states of plastocyanin and ferredoxin
    C = [PC], F = [Fd] (ox. forms)
    """
    kLI = (1 - ps2cs) * pfd

    y0 = (
        KeqC
        * KeqF
        * PCred
        * PSItot
        * kPCox
        * (Fd * kFdred + O2 * k0)
        / (
            Fd * KeqC * KeqF * PCred * kFdred * kPCox
            + Fd * KeqF * kFdred * (KeqC * kLI + PC * kPCox)
            + Fdred * kFdred * (KeqC * kLI + PC * kPCox)
            + KeqC * KeqF * O2 * PCred * k0 * kPCox
            + KeqC * KeqF * PCred * kLI * kPCox
            + KeqF * O2 * k0 * (KeqC * kLI + PC * kPCox)
        )
    )

    y1 = (
        PSItot
        * (Fdred * kFdred * (KeqC * kLI + PC * kPCox) + KeqC * KeqF * PCred * kLI * kPCox)
        / (
            Fd * KeqC * KeqF * PCred * kFdred * kPCox
            + Fd * KeqF * kFdred * (KeqC * kLI + PC * kPCox)
            + Fdred * kFdred * (KeqC * kLI + PC * kPCox)
            + KeqC * KeqF * O2 * PCred * k0 * kPCox
            + KeqC * KeqF * PCred * kLI * kPCox
            + KeqF * O2 * k0 * (KeqC * kLI + PC * kPCox)
        )
    )
    y2 = PSItot - y0 - y1

    return y0, y1, y2


def vFd_red(
    Fd: float,
    Fdred: float,
    A1: float,
    A2: float,
    kFdred: float,
    Keq_FAFd: float,
) -> float:
    """rate of the redcution of Fd by the activity of PSI
    used to be equall to the rate of PSI but now
    alternative electron pathway from Fd allows for the production of ROS
    hence this rate has to be separate
    """
    return kFdred * Fd * A1 - kFdred / Keq_FAFd * Fdred * A2


def vAscorbate(
    A: float,
    H: float,
    kf1: float,
    kr1: float,
    kf2: float,
    kr2: float,
    kf3: float,
    kf4: float,
    kr4: float,
    kf5: float,
    XT: float,
) -> float:
    """lumped reaction of ascorbate peroxidase
    the cycle stretched to a linear chain with
    two steps producing the MDA
    two steps releasing ASC
    and one step producing hydrogen peroxide"""
    nom = A * H * XT
    denom = (
        A * H * (1 / kf3 + 1 / kf5)
        + A / kf1
        + H / kf4
        + H * kr4 / (kf4 * kf5)
        + H / kf2
        + H * kr2 / (kf2 * kf3)
        + kr1 / (kf1 * kf2)
        + kr1 * kr2 / (kf1 * kf2 * kf3)
    )
    return nom / denom


def vMDAreduct(
    NADPH: float,
    MDA: float,
    kcatMDAR: float,
    KmMDAR_NADPH: float,
    KmMDAR_MDA: float,
    MDAR0: float,
) -> float:
    """Compare Valero et al. 2016"""
    nom = kcatMDAR * MDAR0 * NADPH * MDA
    denom = (
        KmMDAR_NADPH * MDA + KmMDAR_MDA * NADPH + NADPH * MDA + KmMDAR_NADPH * KmMDAR_MDA
    )
    return nom / denom


def vMehler(
    A: float,
    O2ext: float,
    kMehler: float,
) -> float:
    """Draft Mehler reaction inspired from PSI reaction.
    This reaction is lumping the reduction of O2 instead of Fd
    resulting in Superoxide, as well as the Formation of H2O2 in one reaction.
    The entire reaction is scaled by the arbitrary parameter kMehler
    """
    return A * kMehler * O2ext


def vGR(
    NADPH: float,
    GSSG: float,
    kcat_GR: float,
    GR0: float,
    KmNADPH: float,
    KmGSSG: float,
) -> float:
    nom = kcat_GR * GR0 * NADPH * GSSG
    denom = KmNADPH * GSSG + KmGSSG * NADPH + NADPH * GSSG + KmNADPH * KmGSSG
    return nom / denom


def vDHAR(
    DHA: float,
    GSH: float,
    kcat_DHAR: float,
    DHAR0: float,
    KmDHA: float,
    K: float,
    KmGSH: float,
) -> float:
    nom = kcat_DHAR * DHAR0 * DHA * GSH
    denom = K + KmDHA * GSH + KmGSH * DHA + DHA * GSH
    return nom / denom


def v3ASC(
    MDA: float,
    k3: float,
) -> float:
    return k3 * MDA**2


def add_mehler(m: Model, c_chl: str, tissue: str) -> Model:
    m.add_parameters(
        {
            "kf1": 10000.0,
            "kr1": 220.0,
            "kf2": 10000.0,
            "kr2": 4000.0,
            "kf3": 2510.0,  #
            "kf4": 10000.0,
            "kr4": 4000.0,
            "kf5": 2510.0,  #
            "XT": 0.07,  # according to Valero
            "kMehler": 1.0,
            # V09 µM -> mM
            "kcat_GR": 595,
            "kcat_DHAR": 142,
            "k1APX": 12 / 1e-3,
            "k2APX": 50 / 1e-3,
            "k3APX": 2.1 / 1e-3,
            "k4APX": 0.7 / 1e-3,
            "k5APX": 0.01,
            "k3": 0.5 / 1e-3,
            "k4": 0.1 / 1e-3,
            "k5": 0.2 / 1e-3,
            "k6": 0.2 / 1e-3,
            "k7": 0.7 / 1e-3,
            "k8": 2e-6 / 1e-3,
            "KmNADPH": 3e-3,
            "KmGSSG": 2e2 * 1e-3,
            "KmDHA": 70e-3,
            "KmGSH": 2.5e3 * 1e-3,
            "K": 5e5 * (1e-3) ** 2,  # ?
            "GR0": 1.4e-3,
            "DHAR0": 1.7e-3,
            "APX0": 70e-3,
            "Glutathion_total": 10,
            "Ascorbate_total": 10,
            # V16 µM->mM and h->s
            "kcatMDAR": 1080000 / (60 * 60),
            "KmMDAR_NADPH": 23e-3,
            "KmMDAR_MDA": 1.4e-3,
            "MDAR0": 2e-3,
        }
    )
    m.add_compounds(
        [
            f"MDA{c_chl}{tissue}",
            f"H2O2{c_chl}{tissue}",
            f"DHA{c_chl}{tissue}",
            f"GSSG{c_chl}{tissue}",
        ]
    )

    m.add_algebraic_module(
        module_name=f"ascorbate_moiety{c_chl}{tissue}",
        function=moiety_2,
        derived_compounds=[f"ASC{c_chl}{tissue}"],
        compounds=[f"MDA{c_chl}{tissue}", f"DHA{c_chl}{tissue}"],
        parameters=["Ascorbate_total"],
    )

    m.add_algebraic_module(
        module_name=f"glutathion_moiety{c_chl}{tissue}",
        function=moiety_2,
        derived_compounds=[f"GSH{c_chl}{tissue}"],
        compounds=[f"GSSG{c_chl}{tissue}", f"GSSG{c_chl}{tissue}"],
        parameters=["Glutathion_total"],
    )

    m.update_algebraic_module(
        module_name=f"ps1states{c_chl}{tissue}",
        function=ps1analytic_mehler,
        derived_compounds=[
            f"A0{c_chl}{tissue}",
            f"A1{c_chl}{tissue}",
            f"A2{c_chl}{tissue}",
        ],
        compounds=[
            f"PC{c_chl}{tissue}",
            f"PCred{c_chl}{tissue}",
            f"Fd{c_chl}{tissue}",
            f"Fdred{c_chl}{tissue}",
            f"ps2cs{c_chl}{tissue}",
        ],
        parameters=[
            "PSItot",
            "kFdred",
            "Keq_FAFd",
            "Keq_PCP700",
            "kPCox",
            "pfd",
            "kMehler",
            "O2ext",
        ],
        args=[
            f"PC{c_chl}{tissue}",
            f"PCred{c_chl}{tissue}",
            f"Fd{c_chl}{tissue}",
            f"Fdred{c_chl}{tissue}",
            f"ps2cs{c_chl}{tissue}",
            "PSItot",
            "kFdred",
            "Keq_FAFd",
            "Keq_PCP700",
            "kPCox",
            "pfd",
            "kMehler",
            "O2ext",
        ],
    )

    m.update_reaction(
        rate_name=f"vPS1{c_chl}{tissue}",
        stoichiometry={f"PC{c_chl}{tissue}": 1},
        modifiers=[f"A0{c_chl}{tissue}", f"ps2cs{c_chl}{tissue}"],
        dynamic_variables=[f"A0{c_chl}{tissue}", f"ps2cs{c_chl}{tissue}"],
        parameters=["pfd"],
    )

    m.add_reaction(
        rate_name=f"vFdred{c_chl}{tissue}",
        function=vFd_red,
        stoichiometry={f"Fd{c_chl}{tissue}": -1},
        modifiers=[
            f"Fdred{c_chl}{tissue}",
            f"A1{c_chl}{tissue}",
            f"A2{c_chl}{tissue}",
        ],
        parameters=["kFdred", "Keq_FAFd"],
    )

    m.add_reaction(
        rate_name=f"vAscorbate{c_chl}{tissue}",
        function=vAscorbate,
        stoichiometry={
            f"H2O2{c_chl}{tissue}": -1,
            f"MDA{c_chl}{tissue}": 2,
        },
        modifiers=[f"ASC{c_chl}{tissue}"],
        dynamic_variables=[f"ASC{c_chl}{tissue}", f"H2O2{c_chl}{tissue}"],
        parameters=[
            "kf1",
            "kr1",
            "kf2",
            "kr2",
            "kf3",
            "kf4",
            "kr4",
            "kf5",
            "XT",
        ],
    )

    m.add_reaction(
        rate_name=f"vMDAreduct{c_chl}{tissue}",
        function=vMDAreduct,
        stoichiometry={f"NADPH{c_chl}{tissue}": -1, f"MDA{c_chl}{tissue}": -2},
        parameters=["kcatMDAR", "KmMDAR_NADPH", "KmMDAR_MDA", "MDAR0"],
    )

    m.add_reaction(
        rate_name=f"vMehler{c_chl}{tissue}",
        function=vMehler,
        stoichiometry={
            f"H2O2{c_chl}{tissue}": 1 * m.get_parameter("convf")
        },  # required to convert as rates of PSI are expressed in mmol/mol Chl
        modifiers=[f"A1{c_chl}{tissue}"],
        parameters=["O2ext", "kMehler"],
    )

    m.add_reaction(
        rate_name=f"vGR{c_chl}{tissue}",
        function=vGR,
        stoichiometry={f"NADPH{c_chl}{tissue}": -1, f"GSSG{c_chl}{tissue}": -1},
        dynamic_variables=[f"NADPH{c_chl}{tissue}", f"GSSG{c_chl}{tissue}"],
        parameters=["kcat_GR", "GR0", "KmNADPH", "KmGSSG"],
    )

    m.add_reaction(
        rate_name=f"vDHAR{c_chl}{tissue}",
        function=vDHAR,
        stoichiometry={f"DHA{c_chl}{tissue}": -1, f"GSSG{c_chl}{tissue}": 1},
        modifiers=[f"GSH{c_chl}{tissue}"],
        dynamic_variables=[f"DHA{c_chl}{tissue}", f"GSH{c_chl}{tissue}"],
        parameters=["kcat_DHAR", "DHAR0", "KmDHA", "K", "KmGSH"],
    )

    m.add_reaction(
        rate_name=f"v3ASC{c_chl}{tissue}",
        function=v3ASC,
        stoichiometry={
            f"MDA{c_chl}{tissue}": -2,
            f"DHA{c_chl}{tissue}": 1,
        },
        dynamic_variables=[f"MDA{c_chl}{tissue}"],
        parameters=["k3"],
    )
    return m
