from .shared_rate_functions import (
    mass_action_1s,
    reversible_mass_action_keq_1s_1p,
    reversible_mass_action_keq_1s_2p,
    reversible_mass_action_keq_2s_1p,
    reversible_mass_action_keq_2s_2p,
    reversible_mass_action_keq_2s_3p,
    reversible_mass_action_keq_3s_3p,
)
from modelbase.ode import Model


def _add_hydroxyaspartate_aldolase(
    model: Model,
    c_per: str,
    tissue1: str,
) -> None:
    """Glyoxylate(aq) + Glycine(aq) ⇌ 3-hydroxyaspartate(aq)"""
    model.add_parameters(
        {
            "kf_hydroxyaspartate_aldolase": 1,
            "keq_hydroxyaspartate_aldolase": 4,
        }
    )
    model.add_reaction(
        rate_name=f"hydroxyaspartate_aldolase{c_per}{tissue1}",
        function=reversible_mass_action_keq_2s_1p,
        stoichiometry={
            f"GLYX{c_per}{tissue1}": -1,
            f"GLY{c_per}{tissue1}": -1,
            f"hydroxyaspartate{c_per}{tissue1}": 1,
        },
        parameters=[
            "kf_hydroxyaspartate_aldolase",
            "keq_hydroxyaspartate_aldolase",
        ],
        reversible=True,
    )


def _add_iminoaspartate_formation(
    model: Model,
    c_per: str,
    tissue1: str,
) -> None:
    """3-hydroxyaspartate(aq) ⇌ Iminoaspartate(aq) + H2O(l)"""
    model.add_parameters(
        {
            "kf_iminoaspartate_formation": 1,
            "keq_iminoaspartate_formation": 4,
        }
    )
    model.add_reaction(
        rate_name=f"iminoaspartate_formation{c_per}{tissue1}",
        function=reversible_mass_action_keq_1s_1p,
        stoichiometry={
            f"hydroxyaspartate{c_per}{tissue1}": -1,
            f"iminoaspartate{c_per}{tissue1}": 1,
        },
        parameters=[
            "kf_iminoaspartate_formation",
            "keq_iminoaspartate_formation",
        ],
        reversible=True,
    )


def _add_oxaloacetate_formation(
    model: Model,
    c_per: str,
    tissue1: str,
) -> None:
    """Iminoaspartate(aq) + H2O(l) ⇌ Oxaloacetate(aq) + NH3(aq)"""
    model.add_parameters(
        {
            "kf_oxaloacetate_formation": 1,
            "keq_oxaloacetate_formation": 7.1e3,
        }
    )
    model.add_reaction(
        rate_name=f"oxaloacetate_formation{c_per}{tissue1}",
        function=reversible_mass_action_keq_1s_2p,
        stoichiometry={
            f"iminoaspartate{c_per}{tissue1}": -1,
            f"oxaloacetate{c_per}{tissue1}": 1,
        },
        parameters=[
            "NH4_pool",
            "kf_oxaloacetate_formation",
            "keq_oxaloacetate_formation",
        ],
        reversible=True,
    )


def _add_malate_dehydrogenase(
    model: Model, c_chl: str, c_per: str, tissue1: str, tissue2: str
) -> None:
    """Oxaloacetate(aq) + NADH(aq) ⇌ Malate(aq) + NAD(aq)"""
    model.add_parameters(
        {
            "kf_malate_dehydrogenase": 1,
            "keq_malate_dehydrogenase": 4.4e4,
        }
    )

    model.add_reaction(
        rate_name=f"malate_dehydrogenase{c_per}",
        function=reversible_mass_action_keq_2s_2p,
        stoichiometry={
            f"oxaloacetate{c_per}{tissue1}": -1,
            f"NADH{c_chl}{tissue2}": -1,
            f"malate{c_chl}{tissue2}": 1,
            f"NAD{c_chl}{tissue2}": 1,
        },
        parameters=[
            "kf_malate_dehydrogenase",
            "keq_malate_dehydrogenase",
        ],
        args=[
            f"oxaloacetate{c_per}{tissue1}",
            f"NADH{c_chl}{tissue2}",
            f"malate{c_chl}{tissue2}",
            f"NAD{c_chl}{tissue2}",
            "kf_malate_dehydrogenase",
            "keq_malate_dehydrogenase",
        ],
        reversible=True,
    )


def _add_malic_enzyme(model: Model, c_chl: str, tissue2: str) -> None:
    """NAD (aq) + (S)-Malate(aq) + H2O(l) ⇌ NADH(aq) + Pyruvate(aq) + CO2(total)"""
    model.add_parameters(
        {
            "kf_malic_enzyme": 1,
            "keq_malic_enzyme": 0.2,
        }
    )
    model.add_reaction(
        rate_name=f"malic_enzyme{c_chl}{tissue2}",
        function=reversible_mass_action_keq_2s_3p,
        stoichiometry={
            f"malate{c_chl}{tissue2}": -1,
            f"NAD{c_chl}{tissue2}": -1,
            f"pyruvate{c_chl}{tissue2}": 1,
            f"CO2{c_chl}{tissue2}": 1,
            f"NADH{c_chl}{tissue2}": 1,
        },
        parameters=[
            "kf_malic_enzyme",
            "keq_malic_enzyme",
        ],
        reversible=True,
        args=[
            f"malate{c_chl}{tissue2}",
            f"NAD{c_chl}{tissue2}",
            f"pyruvate{c_chl}{tissue2}",
            f"CO2{c_chl}{tissue2}",
            f"NADH{c_chl}{tissue2}",
            "kf_malic_enzyme",
            "keq_malic_enzyme",
        ],
    )


def _add_pyruvate_phosphate_dikinase(model: Model, c_chl: str, tissue2: str) -> None:
    """Pyruvate(aq) + ATP(aq) + Orthophosphate(aq) ⇌ PEP(aq) + AMP(aq) + Diphosphate(aq)"""
    model.add_parameters(
        {
            "kf_pyruvate_phosphate_dikinase": 1,
            "keq_pyruvate_phosphate_dikinase": 9.6e-3,
        }
    )

    model.add_reaction(
        rate_name=f"pyruvate_phosphate_dikinase{c_chl}{tissue2}",
        function=reversible_mass_action_keq_3s_3p,
        stoichiometry={
            f"pyruvate{c_chl}{tissue2}": -1,
            f"ATP{c_chl}{tissue2}": -1,
            f"PEP{c_chl}{tissue2}": 1,
        },
        modifiers=[
            f"Pi{c_chl}{tissue2}",
        ],
        parameters=[
            "AMP_pool",
            "DiPi",
            "kf_pyruvate_phosphate_dikinase",
            "keq_pyruvate_phosphate_dikinase",
        ],
        args=[
            f"pyruvate{c_chl}{tissue2}",
            f"ATP{c_chl}{tissue2}",
            f"Pi{c_chl}{tissue2}",
            f"PEP{c_chl}{tissue2}",
            "AMP_pool",
            "DiPi",
            "kf_pyruvate_phosphate_dikinase",
            "keq_pyruvate_phosphate_dikinase",
        ],
        reversible=True,
    )


def _add_pep_carboxylase(
    model: Model,
    c_chl: str,
    c_per: str,
    tissue1: str,
    tissue2: str,
) -> None:
    """Phosphoenolpyruvate(aq) + CO2(total) ⇌ Orthophosphate(aq) + Oxaloacetate(aq)"""
    model.add_parameters(
        {
            "kf_pep_carboxylase": 1,
            "keq_pep_carboxylase": 4.4e5,
        }
    )

    model.add_reaction(
        rate_name=f"pep_carboxylase{c_chl}{tissue2}",
        function=reversible_mass_action_keq_2s_2p,
        stoichiometry={
            f"PEP{c_chl}{tissue2}": -1,
            f"HCO3{c_chl}{tissue2}": -1,
            f"oxaloacetate{c_per}{tissue1}": 1,
        },
        modifiers=[f"Pi{c_chl}{tissue2}"],
        parameters=[
            "kf_pep_carboxylase",
            "keq_pep_carboxylase",
        ],
        reversible=True,
    )


# TODO: remove me!
def _add_oxaloacetate_outflux(
    model: Model,
    c_chl: str,
    tissue1: str,
) -> None:
    model.add_parameters(
        {
            "k_ex_oxaloacetate": 1,
        }
    )
    model.add_reaction(
        rate_name=f"oxaloacetate_outflux{c_chl}{tissue1}",
        function=mass_action_1s,
        stoichiometry={f"oxaloacetate{c_chl}{tissue1}": -1},
        parameters=["k_ex_oxaloacetate"],
    )


# TODO: properly implement this
def _add_ha_aspartate_shunt(
    model: Model,
    c_per: str,
    tissue1: str,
) -> None:
    model.add_parameters(
        {
            "k_fwd_ha_shunt_1": 1,
            "k_fwd_ha_shunt_2": 1,
        }
    )
    model.add_reaction(
        rate_name=f"ha_aspartate_shunt_1{c_per}{tissue1}",
        function=mass_action_1s,
        stoichiometry={
            f"hydroxyaspartate{c_per}{tissue1}": -1,
            f"aspartate{c_per}{tissue1}": 1,
        },
        parameters=["k_fwd_ha_shunt_1"],
    )
    model.add_reaction(
        rate_name=f"ha_aspartate_shunt_2{c_per}{tissue1}",
        function=mass_action_1s,
        stoichiometry={
            f"asparate{c_per}{tissue1}": -1,
            f"oxaloacetate{c_per}{tissue1}": 1,
        },
        parameters=["k_fwd_ha_shunt_2"],
    )


def add_hydroxyasparate_shunt_rma(
    model: Model,
    c_chl: str,
    c_per: str,
    tissue1: str,
    tissue2: str,
) -> Model:
    """Add c4 bypass with reversible mass-action kinetics"""
    model.add_compounds(
        [
            f"hydroxyaspartate{c_per}{tissue1}",
            f"iminoaspartate{c_per}{tissue1}",
            f"oxaloacetate{c_per}{tissue1}",
            f"malate{c_chl}{tissue2}",
            f"pyruvate{c_chl}{tissue2}",
            f"PEP{c_chl}{tissue2}",
            f"NADH{c_chl}{tissue2}",
            f"NAD{c_chl}{tissue2}",
        ]
    )
    model.add_parameters(
        {
            "DiPi": 0.3,  # mM
            "AMP_pool": 0.001,  # mM, guessed
            "NH4_pool": 1e-6,  # mM
        }
    )
    _add_hydroxyaspartate_aldolase(model, c_per=c_per, tissue1=tissue1)
    _add_iminoaspartate_formation(model, c_per=c_per, tissue1=tissue1)
    _add_oxaloacetate_formation(model, c_per=c_per, tissue1=tissue1)
    _add_malate_dehydrogenase(
        model, c_chl=c_chl, c_per=c_per, tissue1=tissue1, tissue2=tissue2
    )
    _add_malic_enzyme(model, c_chl=c_chl, tissue2=tissue2)
    _add_pyruvate_phosphate_dikinase(model, c_chl=c_chl, tissue2=tissue2)
    _add_pep_carboxylase(
        model, c_chl=c_chl, c_per=c_per, tissue1=tissue1, tissue2=tissue2
    )
    _add_oxaloacetate_outflux(model, c_chl=c_chl, tissue1=tissue1)
    return model
