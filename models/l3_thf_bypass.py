from .shared_rate_functions import reversible_mass_action_keq_2s_2p
from modelbase.ode import Model


def formate_dehydrogenase(
    s1: float, p1: float, s2: float, p2: float, kf: float, keq: float
) -> float:
    return kf * (s1 * s2 - p1 * p2 / keq)


def formate_thf_ligase(
    s1: float,
    s2: float,
    s3: float,
    p1: float,
    p2: float,
    p3: float,
    kf: float,
    keq: float,
) -> float:
    return kf * (s1 * s2 * s3 - p1 * p2 * p3 / keq)


def glycine_hydroxymethyltransferase(
    s1: float,
    s2: float,
    p1: float,
    p2: float,
    kf: float,
    keq: float,
) -> float:
    return kf * (s1 * s2 - p1 * p2 / keq)


def add_thf_bypass_rma(
    model: Model, c_chl: str, c_per: str, c_mit: str, tissue: str
) -> Model:
    """Reversible mass-action"""
    model.add_compounds(
        [
            f"formate{c_mit}{tissue}",
            f"10_formyl_thf{c_mit}{tissue}",
            f"ch2_thf{c_mit}{tissue}",
            f"THF{c_mit}{tissue}",
        ]
    )
    model.add_parameters(
        {
            "kf_formate_dehydrogenase": 1,  # [mM/s]
            "kf_formate_thf_ligase": 1,  # [mM/s]
            "kf_formyl_to_ch2": 1,  # [mM/s]
            "kf_glycine_hydroxymethyltransferase": 1,  # [mM/s]
            "keq_formate_dehydrogenase": 3.4e-3,
            "keq_formate_thf_ligase": 4.73,
            "keq_formyl_to_ch2": 0.787,
            "keq_glycine_hydroxymethyltransferase": 0.072,
            "NAD_pool": 0.6,  # mM
            "NADH_pool": 0.6 * 0.001,  # mM
        }
    )

    model.add_reaction(
        rate_name=f"formate_dehydrogenase{c_mit}{tissue}",
        function=formate_dehydrogenase,
        stoichiometry={
            f"CO2{c_chl}{tissue}": -1,
            # "NADH": -1,
            f"formate{c_mit}{tissue}": 1,
            # "NAD": 1,
        },
        parameters=[
            "NADH_pool",
            "NAD_pool",
            "kf_formate_dehydrogenase",
            "keq_formate_dehydrogenase",
        ],
        reversible=True,
    )

    model.add_reaction(
        rate_name=f"formate_thf_ligase{c_mit}{tissue}",
        function=formate_thf_ligase,
        stoichiometry={
            f"formate{c_mit}{tissue}": -1,
            f"THF{c_mit}{tissue}": -1,
            f"ATP{c_chl}{tissue}": -1,
            f"10_formyl_thf{c_mit}{tissue}": 1,  # 10-Formyltetrahydrofolate
            # "ADP": 1,
            # "Pi": 1,
        },
        modifiers=[
            f"ADP{c_chl}{tissue}",
            f"Pi{c_chl}{tissue}",
        ],
        parameters=[
            "kf_formate_thf_ligase",
            "keq_formate_thf_ligase",
        ],
        reversible=True,
    )

    model.add_reaction(
        rate_name=f"formyl_to_ch2{c_mit}{tissue}",
        function=reversible_mass_action_keq_2s_2p,
        stoichiometry={
            f"10_formyl_thf{c_mit}{tissue}": -1,
            f"NADPH{c_chl}{tissue}": -1,
            f"ch2_thf{c_mit}{tissue}": 1,
            # f"NADP{c_chl}{tissue}": 1,
            # "H2O": 1,
        },
        modifiers=[f"NADP{c_chl}{tissue}"],
        parameters=[
            "kf_formyl_to_ch2",
            "keq_formyl_to_ch2",
        ],
        reversible=True,
    )

    model.add_reaction(
        rate_name=f"glycine_hydroxymethyltransferase{c_mit}{tissue}",
        function=glycine_hydroxymethyltransferase,
        stoichiometry={
            f"ch2_thf{c_mit}{tissue}": -1,
            f"GLY{c_per}{tissue}": -1,
            f"SER{c_per}{tissue}": 1,
            f"THF{c_mit}{tissue}": 1,
        },
        parameters=[
            "kf_glycine_hydroxymethyltransferase",
            "keq_glycine_hydroxymethyltransferase",
        ],
        reversible=True,
    )
    return model
