import numpy as np
from .shared_algebraic_functions import moiety_1
from .shared_rate_functions import (
    atp_synthase,
    b6f,
    cyclic_electron_flow,
    fluorescence,
    fnr,
    keq_atp,
    keq_cytb6f,
    keq_faf_d,
    keq_fnr,
    keq_pcp700,
    keq_pq_red,
    leak,
    mass_action_1s,
    ph_lumen,
    proportional,
    protonation_hill,
    ps1,
    ps1states,
    ps2,
    ps2_crosssection,
    ps2states,
    ptox,
    quencher,
    state_transition_ps1_ps2,
)
from modelbase.ode import Model
from typing import Tuple


def _add_derived_parameters(model: Model) -> None:
    model.add_parameters(
        {
            # standard redox potentials (at pH=0) in V
            "E0_QA": -0.140,
            "E0_PQ": 0.354,
            "E0_PC": 0.380,
            "E0_P700": 0.480,
            "E0_FA": -0.550,
            "E0_Fd": -0.430,
            "E0_NADP": -0.113,
        }
    )
    model.add_derived_parameter("RT", proportional, parameters=["R", "T"])

    model.add_derived_parameter(
        parameter_name="dG_pH",
        function=lambda r, t: np.log(10) * r * t,
        parameters=["R", "T"],
    )

    model.add_derived_parameter(
        parameter_name="Keq_PQred",
        function=keq_pq_red,
        parameters=["E0_QA", "F", "E0_PQ", "pHstroma", "dG_pH", "RT"],
    )

    model.add_derived_parameter(
        parameter_name="Keq_FAFd",
        function=keq_faf_d,
        parameters=["E0_FA", "F", "E0_Fd", "RT"],
    )

    model.add_derived_parameter(
        parameter_name="Keq_PCP700",
        function=keq_pcp700,
        parameters=["E0_PC", "F", "E0_P700", "RT"],
    )
    model.add_derived_parameter(
        parameter_name="Keq_FNR",
        function=keq_fnr,
        parameters=["E0_Fd", "F", "E0_NADP", "pHstroma", "dG_pH", "RT"],
    )


def _add_moieties(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "PQtot": 17.5,  # [mmol/molChl]
            # Bohme1987 but other sources give different values
            # seems to depend greatly on organism and conditions
            "PCtot": 4.0,
            "Fdtot": 5.0,  # Bohme1987
            "APtot": 2.55,  # [mmol/molChl] Bionumbers ~2.55mM (=81mmol/molChl)
            "lhc_total": 1,
            "NADPtot": 0.8,  # estimate from ~ 0.8 mM, Heineke1991
            "Psbstot": 1.0,  # relative pool of PsbS
            "Xtot": 1.0,  # relative pool of carotenoids (V+A+Z)
        }
    )
    model.add_algebraic_module(
        module_name=f"pq_alm{c_chl}{tissue}",
        function=moiety_1,
        derived_compounds=[f"PQred{c_chl}{tissue}"],
        compounds=[f"PQ{c_chl}{tissue}"],
        parameters=["PQtot"],
    )

    model.add_algebraic_module(
        module_name=f"pc_alm{c_chl}{tissue}",
        function=moiety_1,
        derived_compounds=[f"PCred{c_chl}{tissue}"],
        compounds=[f"PC{c_chl}{tissue}"],
        parameters=["PCtot"],
    )

    model.add_algebraic_module(
        module_name=f"fd_alm{c_chl}{tissue}",
        function=moiety_1,
        derived_compounds=[f"Fdred{c_chl}{tissue}"],
        compounds=[f"Fd{c_chl}{tissue}"],
        parameters=["Fdtot"],
    )

    model.add_algebraic_module(
        module_name=f"adp_alm{c_chl}{tissue}",
        function=moiety_1,
        derived_compounds=[f"ADP{c_chl}{tissue}"],
        compounds=[f"ATP{c_chl}{tissue}"],
        parameters=["APtot"],
    )

    model.add_algebraic_module(
        module_name=f"nadp_alm{c_chl}{tissue}",
        function=moiety_1,
        derived_compounds=[f"NADP{c_chl}{tissue}"],
        compounds=[f"NADPH{c_chl}{tissue}"],
        parameters=["NADPtot"],
    )

    model.add_algebraic_module(
        module_name=f"lhc_alm{c_chl}{tissue}",
        function=moiety_1,
        derived_compounds=[f"LHCp{c_chl}{tissue}"],
        compounds=[f"LHC{c_chl}{tissue}"],
        parameters=["lhc_total"],
    )

    model.add_algebraic_module(
        module_name=f"xantophylls_alm{c_chl}{tissue}",
        function=moiety_1,
        derived_compounds=[f"Zx{c_chl}{tissue}"],
        compounds=[f"Vx{c_chl}{tissue}"],
        parameters=["Xtot"],
    )

    model.add_algebraic_module(
        module_name=f"psbs_alm{c_chl}{tissue}",
        function=moiety_1,
        derived_compounds=[f"Psbsp{c_chl}{tissue}"],
        compounds=[f"Psbs{c_chl}{tissue}"],
        parameters=["Psbstot"],
    )


def _add_ps2_cross_section(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            # corresponds to PSI - LHCI supercomplex
            # when chlorophyll decreases more relative fixed antennae
            "staticAntI": 0.37,
            "staticAntII": 0.1,  # corresponds to PSII core
        }
    )
    model.add_algebraic_module(
        module_name=f"ps2crosssection{c_chl}{tissue}",
        function=ps2_crosssection,
        derived_compounds=[f"ps2cs{c_chl}{tissue}"],
        compounds=[f"LHC{c_chl}{tissue}"],
        parameters=["staticAntII", "staticAntI"],
    )


def _add_quencher(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "gamma0": 0.1,  # slow quenching of (Vx - protonation)
            "gamma1": 0.25,  # fast quenching (Vx + protonation)
            "gamma2": 0.6,  # fastest possible quenching (Zx + protonation)
            "gamma3": 0.15,  # slow quenching of Zx present (Zx - protonation)
            "kZSat": 0.12,  # [-] half-saturation constant (relative conc. of Z) for quenching of Z
        }
    )
    model.add_algebraic_module(
        module_name=f"quencher{c_chl}{tissue}",
        function=quencher,
        derived_compounds=[f"Q{c_chl}{tissue}"],
        compounds=[
            f"Psbs{c_chl}{tissue}",
            f"Vx{c_chl}{tissue}",
            f"Psbsp{c_chl}{tissue}",
            f"Zx{c_chl}{tissue}",
        ],
        parameters=["gamma0", "gamma1", "gamma2", "gamma3", "kZSat"],
    )


def _add_photosystems(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "PSIItot": 2.5,  # [mmol/molChl] total concentration of PSII
            "PSItot": 2.5,
            "kH0": 5e8,  # base quenching" after calculation with Giovanni
            "kPQred": 250.0,  # [1/(s*(mmol/molChl))]
            "kPCox": 2500.0,  # a rough estimate: half life of PC->P700 should be ~0.2ms
            "kFdred": 2.5e5,  # a rough estimate: half life of PC->P700 should be ~2micro-s
            # charge separation limiting step ~ 200ps
            "k2": 5e9,
            "kH": 5e9,
            "kF": 6.25e8,  # fluorescence 16ns
        }
    )
    model.add_algebraic_module(
        module_name=f"ps2states{c_chl}{tissue}",
        function=ps2states,
        derived_compounds=[
            f"B0{c_chl}{tissue}",
            f"B1{c_chl}{tissue}",
            f"B2{c_chl}{tissue}",
            f"B3{c_chl}{tissue}",
        ],
        compounds=[
            f"PQ{c_chl}{tissue}",
            f"PQred{c_chl}{tissue}",
            f"ps2cs{c_chl}{tissue}",
            f"Q{c_chl}{tissue}",
        ],
        parameters=[
            "PSIItot",
            "k2",
            "kF",
            "kH",
            "Keq_PQred",  # derived
            "kPQred",
            "pfd",
            "kH0",
        ],
    )

    model.add_algebraic_module(
        module_name=f"ps1states{c_chl}{tissue}",
        function=ps1states,
        derived_compounds=[f"A1{c_chl}{tissue}"],
        compounds=[
            f"PC{c_chl}{tissue}",
            f"PCred{c_chl}{tissue}",
            f"Fd{c_chl}{tissue}",
            f"Fdred{c_chl}{tissue}",
            f"ps2cs{c_chl}{tissue}",
        ],
        parameters=[
            "PSItot",
            "kFdred",
            "Keq_FAFd",
            "Keq_PCP700",
            "kPCox",
            "pfd",
        ],
    )
    model.add_reaction(
        rate_name=f"vPS2{c_chl}{tissue}",
        function=ps2,
        stoichiometry={
            f"PQ{c_chl}{tissue}": -1,
            f"H_lumen{c_chl}{tissue}": 2 / model.get_parameter("bH"),
        },
        modifiers=[f"B1{c_chl}{tissue}"],
        dynamic_variables=[f"B1{c_chl}{tissue}"],  # doesn't depend on PQ
        parameters=["k2"],
    )

    model.add_parameters({})
    model.add_reaction(
        rate_name=f"vPS1{c_chl}{tissue}",
        function=ps1,
        stoichiometry={f"Fd{c_chl}{tissue}": -1, f"PC{c_chl}{tissue}": 1},
        modifiers=[f"A1{c_chl}{tissue}", f"ps2cs{c_chl}{tissue}"],
        dynamic_variables=[
            f"A1{c_chl}{tissue}",
            f"ps2cs{c_chl}{tissue}",
        ],  # doesn't depend on Fd
        parameters=["pfd"],
    )
    model.add_algebraic_module(
        module_name=f"fluorescence{c_chl}{tissue}",
        function=fluorescence,
        derived_compounds=[f"Fluo{c_chl}{tissue}"],
        compounds=[
            f"Q{c_chl}{tissue}",
            f"B0{c_chl}{tissue}",
            f"B2{c_chl}{tissue}",
            f"ps2cs{c_chl}{tissue}",
        ],
        parameters=[
            "k2",  # see _add_photosystems
            "kF",
            "kH",  # see _add_photosystems
        ],
    )


def _add_ph_lumen(model: Model, c_chl: str, tissue: str) -> None:
    model.add_algebraic_module(
        module_name=f"ph_lumen{c_chl}{tissue}",
        function=ph_lumen,
        derived_compounds=[f"pH_lumen{c_chl}{tissue}"],
        compounds=[f"H_lumen{c_chl}{tissue}"],
    )


def _add_ptox(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "kPTOX": 0.01,  # ~ 5 electrons / seconds. This gives a bit more (~20)
        }
    )

    model.add_reaction(
        rate_name=f"vPTOX{c_chl}{tissue}",
        function=ptox,
        stoichiometry={f"PQ{c_chl}{tissue}": 1},
        modifiers=[f"PQred{c_chl}{tissue}"],
        parameters=["kPTOX", "O2ext"],
    )


def _add_ndh(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            # re-introduce e- into PQ pool.
            # Only positive for anaerobic (reducing) condition
            "kNDH": 0.002,
        }
    )

    model.add_reaction(
        rate_name=f"vNDH{c_chl}{tissue}",
        function=mass_action_1s,
        stoichiometry={f"PQ{c_chl}{tissue}": -1},
        parameters=["kNDH"],
    )


def _add_b6f(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            # a rough estimate: transfer PQ->cytf should be ~10ms
            "kCytb6f": 2.5,
        }
    )
    model.add_algebraic_module(
        module_name=f"Keq_B6f{c_chl}{tissue}",
        function=keq_cytb6f,
        derived_compounds=[f"Keq_B6f{c_chl}{tissue}"],
        compounds=[f"pH_lumen{c_chl}{tissue}"],
        parameters=["F", "E0_PQ", "E0_PC", "pHstroma", "RT", "dG_pH"],
    )

    model.add_reaction(
        rate_name=f"vB6f{c_chl}{tissue}",
        function=b6f,
        stoichiometry={
            f"PC{c_chl}{tissue}": -2,
            f"PQ{c_chl}{tissue}": 1,
            f"H_lumen{c_chl}{tissue}": 4 / model.get_parameter("bH"),
        },
        modifiers=[
            f"PQred{c_chl}{tissue}",
            f"PCred{c_chl}{tissue}",
            f"Keq_B6f{c_chl}{tissue}",
        ],
        dynamic_variables=[
            f"PC{c_chl}{tissue}",
            f"PQ{c_chl}{tissue}",
            f"PQred{c_chl}{tissue}",
            f"PCred{c_chl}{tissue}",
            f"Keq_B6f{c_chl}{tissue}",
        ],
        parameters=["kCytb6f"],
        reversible=True,
    )


def _add_cyclic_electron_flow(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "kcyc": 1.0,
        }
    )

    model.add_reaction(
        rate_name=f"vCyc{c_chl}{tissue}",
        function=cyclic_electron_flow,
        stoichiometry={f"PQ{c_chl}{tissue}": -1, f"Fd{c_chl}{tissue}": 2},
        modifiers=[f"Fdred{c_chl}{tissue}"],
        parameters=["kcyc"],
    )


def _add_fnr(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "KM_FNR_F": 1.56,  # corresponds to 0.05 mM (Aliverti1990)
            "KM_FNR_N": 0.22,  # corresponds to 0.007 mM (Shin1971 Aliverti2004)
            "EFNR": 3.0,  # Bohme1987
            "kcatFNR": 500.0,  # Carrillo2003 (kcat~500 1/s)
        }
    )
    model.add_reaction(
        rate_name=f"vFNR{c_chl}{tissue}",
        function=fnr,
        stoichiometry={
            f"Fd{c_chl}{tissue}": 2,
            f"NADPH{c_chl}{tissue}": 1 * model.get_parameter("convf"),
        },
        modifiers=[
            f"Fd{c_chl}{tissue}",
            f"Fdred{c_chl}{tissue}",
            f"NADPH{c_chl}{tissue}",
            f"NADP{c_chl}{tissue}",
        ],
        dynamic_variables=[
            f"Fd{c_chl}{tissue}",
            f"Fdred{c_chl}{tissue}",
            f"NADPH{c_chl}{tissue}",
            f"NADP{c_chl}{tissue}",
        ],
        parameters=[
            "KM_FNR_F",
            "KM_FNR_N",
            "EFNR",
            "kcatFNR",
            "Keq_FNR",  # derived
            "convf",
        ],
    )


def _add_state_transitions(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "kStt7": 0.0035,  # [s-1] fitted to the FM dynamics
            "KM_ST": 0.2,  # Switch point (half-activity of Stt7) for 20% PQ oxidised (80% reduced)
            "n_ST": 2.0,  # Hill coefficient of 4 -> 1/(2.5^4)~1/40 activity at PQox=PQred
            "kPph1": 0.0013,  # [s-1] fitted to the FM dynamics
        }
    )

    model.add_reaction(
        rate_name=f"vSt12{c_chl}{tissue}",
        function=state_transition_ps1_ps2,
        stoichiometry={f"LHC{c_chl}{tissue}": -1},
        modifiers=[f"PQ{c_chl}{tissue}"],
        parameters=["kStt7", "PQtot", "KM_ST", "n_ST"],
    )

    model.add_reaction(
        rate_name=f"vSt21{c_chl}{tissue}",
        function=mass_action_1s,
        stoichiometry={f"LHC{c_chl}{tissue}": 1},
        modifiers=[f"LHCp{c_chl}{tissue}"],
        dynamic_variables=[f"LHCp{c_chl}{tissue}"],
        parameters=["kPph1"],
    )


def _add_atp_synthase(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "kATPsynth": 20.0,  # taken from MATLAB
            "Pi_mol": 0.01,
            "HPR": 14.0 / 3.0,  # Vollmar et al. 2009 (after Zhu et al. 2013)
            "DeltaG0_ATP": 30.6,  # 30.6kJ/mol / RT
        }
    )
    model.add_algebraic_module(
        module_name=f"Keq_ATPsynthase{c_chl}{tissue}",
        function=keq_atp,
        derived_compounds=[f"Keq_ATPsynthase{c_chl}{tissue}"],
        compounds=[f"pH_lumen{c_chl}{tissue}"],
        parameters=["DeltaG0_ATP", "dG_pH", "HPR", "pHstroma", "Pi_mol", "RT"],
    )
    model.add_reaction(
        rate_name=f"vATPsynthase{c_chl}{tissue}",
        function=atp_synthase,
        stoichiometry={
            f"H_lumen{c_chl}{tissue}": -model.get_parameter("HPR")
            / model.get_parameter("bH"),
            f"ATP{c_chl}{tissue}": 1 * model.get_parameter("convf"),
        },
        modifiers=[
            f"ADP{c_chl}{tissue}",
            f"Keq_ATPsynthase{c_chl}{tissue}",
        ],
        dynamic_variables=[
            f"ATP{c_chl}{tissue}",
            f"ADP{c_chl}{tissue}",
            f"Keq_ATPsynthase{c_chl}{tissue}",
        ],
        parameters=[
            "kATPsynth",
            "convf",
        ],
        reversible=True,
    )


def _add_epoxidase(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "kHillX": 5.0,  # [-] hill-coefficient for activity of de-epoxidase
            "kDeepoxV": 0.0024,
            "kEpoxZ": 0.00024,  # 6.e-4        # converted to [1/s]
            # [-] half-saturation pH value for activity de-epoxidase
            # highest activity at ~pH 5.8
            "kphSat": 5.8,
        }
    )
    model.add_reaction(
        rate_name=f"vDeepox{c_chl}{tissue}",
        function=protonation_hill,
        stoichiometry={f"Vx{c_chl}{tissue}": -1},
        modifiers=[f"H_lumen{c_chl}{tissue}"],
        parameters=["kHillX", "kDeepoxV", "kphSat"],
    )

    model.add_reaction(
        rate_name=f"vEpox{c_chl}{tissue}",
        function=mass_action_1s,
        stoichiometry={f"Vx{c_chl}{tissue}": 1},
        modifiers=[f"Zx{c_chl}{tissue}"],
        parameters=["kEpoxZ"],
    )


def _add_lhc_protonation(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "kHillL": 3.0,  # [-] hill-coefficient for activity of de-epoxidase
            "kProtonationL": 0.0096,
            "kphSatLHC": 5.8,
            "kDeprotonation": 0.0096,
        }
    )
    model.add_reaction(
        rate_name=f"vLhcprotonation{c_chl}{tissue}",
        function=protonation_hill,
        stoichiometry={f"Psbs{c_chl}{tissue}": -1},
        modifiers=[f"H_lumen{c_chl}{tissue}"],
        parameters=["kHillL", "kProtonationL", "kphSatLHC"],
    )
    model.add_reaction(
        rate_name=f"vLhcdeprotonation{c_chl}{tissue}",
        function=mass_action_1s,
        stoichiometry={f"Psbs{c_chl}{tissue}": 1},
        modifiers=[f"Psbsp{c_chl}{tissue}"],
        parameters=["kDeprotonation"],
    )


def _add_proton_leak(model: Model, c_chl: str, tissue: str) -> None:
    model.add_parameters(
        {
            "kLeak": 10.0,  # 0.010, # [1/s] leakage rate -- inconsistency with Kathrine
        }
    )

    model.add_reaction(
        rate_name=f"vLeak{c_chl}{tissue}",
        function=leak,
        stoichiometry={f"H_lumen{c_chl}{tissue}": -1 / model.get_parameter("bH")},
        parameters=["kLeak", "pHstroma"],
    )


def get_matuszynska(
    c_chl: str = "",  # _chl
    tissue: str = "",  # _meso or _bs
) -> Model:
    model = Model()
    model.add_compounds(
        [
            f"PQ{c_chl}{tissue}",  # oxidised plastoquinone, membrane
            f"PC{c_chl}{tissue}",  # oxidised plastocyan, membrane / lumen
            f"Fd{c_chl}{tissue}",  # oxidised ferrodoxin, membrane / stroma
            f"ATP{c_chl}{tissue}",  # stromal concentration of ATP, stroma
            f"NADPH{c_chl}{tissue}",  # stromal concentration of NADPH, stroma
            f"H_lumen{c_chl}{tissue}",  # lumenal protons, lumen
            f"LHC{c_chl}{tissue}",  # ,  # non-phosphorylated antenna, membrane
            f"Psbs{c_chl}{tissue}",  # PsBs, membrane / stroma
            f"Vx{c_chl}{tissue}",  # vioolaxathin relative concentration, membrane
        ]
    )
    model.add_parameters(
        {
            "convf": 3.2 * 10e-3,  # converts ATP and NADPH
            "pHstroma": 7.9,
            "bH": 100.0,  # proton buffer: ratio total / free protons
            "O2ext": 8.0,  # corresponds to 250 microM cor to 20%"EFNR": 3.0,  # Bohme1987
            "pfd": 100.0,
            # physical constants
            "F": 96.485,  # Faraday constant
            "R": 8.3e-3,  # universal gas constant
            "T": 298.0,  # Temperature in K - for now assumed to be constant at 25 C
        }
    )
    _add_derived_parameters(model)
    _add_moieties(model, c_chl=c_chl, tissue=tissue)
    _add_ps2_cross_section(model, c_chl=c_chl, tissue=tissue)
    _add_quencher(model, c_chl=c_chl, tissue=tissue)
    _add_photosystems(model, c_chl=c_chl, tissue=tissue)
    _add_ph_lumen(model, c_chl=c_chl, tissue=tissue)
    _add_ptox(model, c_chl=c_chl, tissue=tissue)
    _add_ndh(model, c_chl=c_chl, tissue=tissue)
    _add_b6f(model, c_chl=c_chl, tissue=tissue)
    _add_cyclic_electron_flow(model, c_chl=c_chl, tissue=tissue)
    _add_fnr(model, c_chl=c_chl, tissue=tissue)
    _add_state_transitions(model, c_chl=c_chl, tissue=tissue)
    _add_epoxidase(model, c_chl=c_chl, tissue=tissue)
    _add_lhc_protonation(model, c_chl=c_chl, tissue=tissue)
    _add_atp_synthase(model, c_chl=c_chl, tissue=tissue)
    _add_proton_leak(model, c_chl=c_chl, tissue=tissue)
    return model


def add_matuszynska(model: Model, c_chl: str, tissue: str) -> Model:
    return model + get_matuszynska(c_chl=c_chl, tissue=tissue)
