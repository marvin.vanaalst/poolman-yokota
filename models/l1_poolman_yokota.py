from .shared_rate_functions import glycerate_kinase, michaelis_menten
from modelbase.ode import Model


def convert_yokota_vmaxes(model: Model) -> Model:
    # Convert vmax parameters mM/hour to mM/second
    for i in (
        "vmax_glycolate_oxidase",
        "vmax_glycine_transaminase",
        "vmax_glycine_decarboxylase",
        "vmax_ser_gly_transaminase",
        "vmax_glycerate_dehydrogenase",
        "vmax_catalase",
    ):
        model.update_parameter(i, model.parameters[i] / 3600)
    return model


def connect_poolman_and_yokota(
    model: Model, c_chl: str, c_per: str, tissue: str
) -> Model:
    """Add required reactions to connect poolman and yokota models

    The yokota model starts at glyc instead of 2PG and ends in
    HPA instead of GLYA.
    """
    model = convert_yokota_vmaxes(model)
    model.add_compounds(
        [
            f"2PG{c_chl}{tissue}",  # 2-phosphoglycolate
            f"GLYA{c_chl}{tissue}",  # glycerate
        ]
    )
    model.add_parameters(
        {
            # Brenda
            "vmax_phosphoglycolate_phosphatase": 292,  # [mM/s], Spinacia oleracea
            "kms_phosphoglycolate_phosphatase": 0.029,  # [mM], Spinacia oleracea
            "vmax_glycerate_kinase": 5.71579,  # [mM/s]
            "kms_glycerate_kinase_glycerate": 0.25,  # [mM]
            "kms_glycerate_kinase_atp": 0.21,  # [mM]
            "ki_glycerate_kinase_pga": 0.36,  # [mM]
        }
    )
    model.update_reaction(
        rate_name=f"phosphoglycolate_phosphatase{c_chl}{tissue}",  # R0
        function=michaelis_menten,
        stoichiometry={
            f"2PG{c_chl}{tissue}": -1,
            f"GLYC{c_chl}{tissue}": 1,
        },
        parameters=[
            "vmax_phosphoglycolate_phosphatase",
            "kms_phosphoglycolate_phosphatase",
        ],
    )
    model.update_reaction(
        rate_name=f"glycerate_dehydrogenase{c_per}{tissue}",  # R5
        function=michaelis_menten,
        stoichiometry={
            f"HPA{c_per}{tissue}": -1,
            f"GLYA{c_chl}{tissue}": 1,
        },  # NAD(H) is ignored
    )

    model.add_reaction(
        rate_name=f"glycerate_kinase{c_chl}{tissue}",
        function=glycerate_kinase,
        stoichiometry={
            f"GLYA{c_chl}{tissue}": -1,
            f"ATP{c_chl}{tissue}": -1,
            f"PGA{c_chl}{tissue}": 1,
            # "ADP": 1,
        },
        modifiers=[f"PGA{c_chl}{tissue}"],
        parameters=[
            "vmax_glycerate_kinase",
            "kms_glycerate_kinase_glycerate",
            "kms_glycerate_kinase_atp",
            "ki_glycerate_kinase_pga",
        ],
    )
    # Clean up unused stuff
    model.remove_parameters(["v_influx"])
    return model
