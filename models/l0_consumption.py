from .shared_rate_functions import mass_action_1s
from modelbase.ode import Model


def add_consumption(model: Model, c_chl: str, tissue: str) -> Model:
    """Generalized consumption for ATP and NADPH.

    This acknowledges that we don't include all ATP and NADPH-consuming
    processes
    """
    model.add_parameters(
        {
            "k_ex_atp": 0.2,
            "k_ex_nadph": 0.2,
        }
    )
    model.add_reaction(
        rate_name=f"atp_consumption{c_chl}{tissue}",
        function=mass_action_1s,
        stoichiometry={f"ATP{c_chl}{tissue}": -1},
        parameters=["k_ex_atp"],
    )
    model.add_reaction(
        rate_name=f"nadph_consumption{c_chl}{tissue}",
        function=mass_action_1s,
        stoichiometry={f"NADPH{c_chl}{tissue}": -1},
        parameters=["k_ex_nadph"],
    )
    return model
