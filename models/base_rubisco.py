"""Since there are so many variants of rubisco in the models
I found it easiest to just add it manually for each of them
at the end
"""

from .utils import filter_element, rename_element
from modelbase.ode import Model


def rubisco_poolman(
    RUBP: float,
    CO2: float,
    PGA: float,
    FBP: float,
    SBP: float,
    P: float,
    NADPH: float,
    O2: float,
    vmax: float,
    kms_rubp: float,
    kms_co2: float,
    ki_pga: float,
    ki_fbp: float,
    ki_sbp: float,
    ki_p: float,
    ki_nadph: float,
    ki_o2: float,
) -> float:
    numerator = vmax * RUBP * CO2
    rubp_inhibition = (
        1
        + (PGA / ki_pga)
        + (FBP / ki_fbp)
        + (SBP / ki_sbp)
        + (P / ki_p)
        + (NADPH / ki_nadph)
    )
    denonimator = (
        RUBP * CO2 + kms_rubp * RUBP * rubp_inhibition + kms_co2 * CO2 * (1 + O2 / ki_o2)
    )
    return numerator / denonimator


def rubisco(
    RUBP: float,
    CO2: float,
    PGA: float,
    FBP: float,
    SBP: float,
    P: float,
    NADPH: float,
    O2: float,
    vmax: float,
    kms_rubp: float,
    kms_co2: float,
    ki_pga: float,
    ki_fbp: float,
    ki_sbp: float,
    ki_p: float,
    ki_nadph: float,
    ki_o2: float,
) -> float:
    numerator = vmax * RUBP * CO2
    rubp_inhibition = (
        1
        + (PGA / ki_pga)
        + (FBP / ki_fbp)
        + (SBP / ki_sbp)
        + (P / ki_p)
        + (NADPH / ki_nadph)
    )
    denonimator = (
        RUBP * CO2 + kms_rubp * RUBP * rubp_inhibition + kms_co2 * CO2 * (1 + O2 / ki_o2)
    )
    return numerator / denonimator


def add_rubisco_carboxylase(model: Model, c_chl: str, tissue: str) -> Model:
    model.add_reaction(
        rate_name=f"rubisco_co2{c_chl}{tissue}",  # v1
        function=rubisco,
        stoichiometry={
            f"RUBP{c_chl}{tissue}": -1,
            f"PGA{c_chl}{tissue}": 2,
        },
        modifiers=[
            f"PGA{c_chl}{tissue}",
            f"FBP{c_chl}{tissue}",
            f"SBP{c_chl}{tissue}",
            f"Pi{c_chl}{tissue}",
        ],
        parameters=[
            "Vmax_rubisco_co2",
            "kms_rubisco_rubp",
            "kms_rubisco_co2",
            "kms_rubisco_o2",
            "Ki_1_1",
            "Ki_1_2",
            "Ki_1_3",
            "Ki_1_4",
            "Ki_1_5",
            "NADPH",
            "CO2",
            "O2",
        ],
        args=[
            f"RUBP{c_chl}{tissue}",
            "CO2",
            f"PGA{c_chl}{tissue}",
            f"FBP{c_chl}{tissue}",
            f"SBP{c_chl}{tissue}",
            f"Pi{c_chl}{tissue}",
            "NADPH",
            "O2",
            "Vmax_rubisco_co2",
            "kms_rubisco_rubp",
            "kms_rubisco_co2",
            "Ki_1_1",
            "Ki_1_2",
            "Ki_1_3",
            "Ki_1_4",
            "Ki_1_5",
            "kms_rubisco_o2",
        ],
    )
    return model


def add_rubisco_oxygenase(model: Model, c_chl: str, tissue: str) -> Model:
    model.add_reaction(
        rate_name=f"rubisco_o2{c_chl}{tissue}",
        function=rubisco,
        stoichiometry={
            f"RUBP{c_chl}{tissue}": -1,
            f"PGA{c_chl}{tissue}": 1,
            f"2PG{c_chl}{tissue}": 1,
        },
        modifiers=[
            f"PGA{c_chl}{tissue}",
            f"FBP{c_chl}{tissue}",
            f"SBP{c_chl}{tissue}",
            f"Pi{c_chl}{tissue}",
        ],
        parameters=[
            "Vmax_rubisco_co2",
            "kms_rubisco_rubp",
            "kms_rubisco_co2",
            "kms_rubisco_o2",
            "Ki_1_1",
            "Ki_1_2",
            "Ki_1_3",
            "Ki_1_4",
            "Ki_1_5",
            "NADPH",
            "CO2",
            "O2",
        ],
        args=[
            f"RUBP{c_chl}{tissue}",
            "O2",
            f"PGA{c_chl}{tissue}",
            f"FBP{c_chl}{tissue}",
            f"SBP{c_chl}{tissue}",
            f"Pi{c_chl}{tissue}",
            "NADPH",
            "CO2",
            "Vmax_rubisco_co2",
            "kms_rubisco_rubp",
            "kms_rubisco_co2",
            "Ki_1_1",
            "Ki_1_2",
            "Ki_1_3",
            "Ki_1_4",
            "Ki_1_5",
            "kms_rubisco_co2",
        ],
    )
    return model


def update_rubisco_carboxylase_dynamic_nadph(
    model: Model, c_chl: str, tissue: str
) -> Model:
    rate_name = f"rubisco_co2{c_chl}{tissue}"
    rate = model.rates[rate_name]
    model.update_reaction(
        rate_name=rate_name,
        modifiers=rate.modifiers + [f"NADPH{c_chl}{tissue}"],
        parameters=filter_element(rate.parameters, "NADPH"),
        args=rename_element(rate.args, "NADPH", f"NADPH{c_chl}{tissue}"),
    )
    return model


def update_rubisco_oxygenase_dynamic_nadph(
    model: Model, c_chl: str, tissue: str
) -> Model:
    rate_name = f"rubisco_o2{c_chl}{tissue}"
    rate = model.rates[rate_name]
    model.update_reaction(
        rate_name=rate_name,
        modifiers=rate.modifiers + [f"NADPH{c_chl}{tissue}"],
        parameters=filter_element(rate.parameters, "NADPH"),
        args=rename_element(rate.args, "NADPH", f"NADPH{c_chl}{tissue}"),
    )
    return model


def update_rubisco_carboxylase_dynamic_co2(
    model: Model, c_chl: str, tissue: str
) -> Model:
    rate_name = f"rubisco_co2{c_chl}{tissue}"
    rate = model.rates[rate_name]
    model.update_reaction(
        rate_name=rate_name,
        stoichiometry={
            f"RUBP{c_chl}{tissue}": -1,
            f"CO2{c_chl}{tissue}": -1,
            f"PGA{c_chl}{tissue}": 2,
        },
        modifiers=rate.modifiers,
        parameters=filter_element(rate.parameters, "CO2"),
        args=rename_element(rate.args, "CO2", f"CO2{c_chl}{tissue}"),
    )
    return model


def update_rubisco_oxygenase_dynamic_co2(model: Model, c_chl: str, tissue: str) -> Model:
    rate_name = f"rubisco_o2{c_chl}{tissue}"
    rate = model.rates[rate_name]
    model.update_reaction(
        rate_name=rate_name,
        modifiers=rate.modifiers + [f"CO2{c_chl}{tissue}"],
        parameters=filter_element(rate.parameters, "CO2"),
        args=rename_element(rate.args, "CO2", f"CO2{c_chl}{tissue}"),
    )
    return model
