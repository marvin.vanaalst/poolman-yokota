from .l1_dynamic_nadph import update_cbb_reactions_dynamic_nadph
from modelbase.ode import Model


def remove_static_atp_production(m: Model, c_chl: str, tissue: str) -> Model:
    m.remove_reaction(f"atp_synthase{c_chl}{tissue}")
    m.remove_algebraic_module(f"ADP_moiety{c_chl}{tissue}")
    m.remove_compound(f"ATP{c_chl}{tissue}")
    m.remove_parameter("Vmax_16")
    return m


def connect_poolman_matuszynska(m: Model, c_chl: str, tissue: str) -> Model:
    m.update_parameters(
        {
            "Phosphate_total": 17.05,
        }
    )
    m = update_cbb_reactions_dynamic_nadph(m, c_chl=c_chl, tissue=tissue)
    return m
