from .base_matuszynska import add_matuszynska
from .base_poolman import add_poolman
from .base_rubisco import (
    add_rubisco_carboxylase,
    add_rubisco_oxygenase,
    update_rubisco_carboxylase_dynamic_co2,
    update_rubisco_carboxylase_dynamic_nadph,
    update_rubisco_oxygenase_dynamic_co2,
    update_rubisco_oxygenase_dynamic_nadph,
)
from .base_yokota import add_yokota
from .l0_consumption import add_consumption
from .l0_mehler import add_mehler
from .l0_thioredoxin import add_thioredoxin_regulation
from .l1_dynamic_co2 import add_dynamic_co2, update_glycine_decarboxylase_dynamic_co2
from .l1_dynamic_nadph import update_yokota_reactions_dynamic_nadph
from .l1_poolman_matuszynska import (
    connect_poolman_matuszynska,
    remove_static_atp_production,
)
from .l1_poolman_yokota import connect_poolman_and_yokota
from .l3_gdc_reroute import add_gcd_reroute
from .l3_hydroxyaspartate_shunt import add_hydroxyasparate_shunt_rma
from .l3_taco_bypass import add_taco_bypass_imm
from .l3_thf_bypass import add_thf_bypass_rma
from .utils import create_model, modify_model, multiply_parameter, remove_parameters
from functools import partial
from modelbase.ode import Model


def get_poolman(
    c_chl: str = "",
    tissue: str = "",
) -> Model:
    return create_model(
        partial(add_poolman, c_chl=c_chl, tissue=tissue),
        partial(add_rubisco_carboxylase, c_chl=c_chl, tissue=tissue),
    )


def get_yokota(
    c_chl: str = "",
    c_per: str = "",
    c_mit: str = "",
    tissue: str = "",
) -> Model:
    return create_model(
        partial(add_yokota, c_chl=c_chl, c_per=c_per, c_mit=c_mit, tissue=tissue),
    )


def get_matuszynska(
    c_chl: str = "",
    tissue: str = "",
) -> Model:
    return create_model(
        partial(add_matuszynska, c_chl=c_chl, tissue=tissue),
    )


def get_poolman_yokota(
    c_chl: str = "",
    c_per: str = "",
    c_mit: str = "",
    tissue: str = "",
) -> Model:
    return create_model(
        partial(add_poolman, c_chl=c_chl, tissue=tissue),
        partial(add_yokota, c_chl=c_chl, c_per=c_per, c_mit=c_mit, tissue=tissue),
        partial(add_rubisco_carboxylase, c_chl=c_chl, tissue=tissue),
        partial(add_rubisco_oxygenase, c_chl=c_chl, tissue=tissue),
        partial(connect_poolman_and_yokota, c_chl=c_chl, c_per=c_per, tissue=tissue),
    )


def get_poolman_matuszynska(
    c_chl: str = "",
    tissue: str = "",
) -> Model:
    enzyme_vmaxes = {
        f"rubisco_co2{c_chl}{tissue}": "Vmax_rubisco_co2",
        f"fbpase{c_chl}{tissue}": "Vmax_6",
        f"sbpase{c_chl}{tissue}": "Vmax_9",
        f"prk{c_chl}{tissue}": "Vmax_13",
        f"starch_production{c_chl}{tissue}": "Vmax_starch",
    }
    return create_model(
        # Poolman
        partial(add_poolman, c_chl=c_chl, tissue=tissue),
        partial(add_rubisco_carboxylase, c_chl=c_chl, tissue=tissue),
        partial(remove_parameters, parameters=["NADPH", "NADP"]),
        partial(remove_static_atp_production, c_chl=c_chl, tissue=tissue),
        # Matuszynska
        partial(add_matuszynska, c_chl=c_chl, tissue=tissue),
        partial(connect_poolman_matuszynska, c_chl=c_chl, tissue=tissue),
        partial(update_rubisco_carboxylase_dynamic_nadph, c_chl=c_chl, tissue=tissue),
        partial(
            add_thioredoxin_regulation,
            enzyme_vmaxes=enzyme_vmaxes,
            c_chl=c_chl,
            tissue=tissue,
        ),
        partial(add_mehler, c_chl=c_chl, tissue=tissue),
        partial(add_consumption, c_chl=c_chl, tissue=tissue),
    )


def get_pmy(c_chl: str = "", c_per: str = "", c_mit: str = "", tissue: str = "") -> Model:
    enzyme_vmaxes = {
        f"rubisco_co2{c_chl}{tissue}": "Vmax_rubisco_co2",
        f"rubisco_o2{c_chl}{tissue}": "Vmax_rubisco_o2",
        f"fbpase{c_chl}{tissue}": "Vmax_6",
        f"sbpase{c_chl}{tissue}": "Vmax_9",
        f"prk{c_chl}{tissue}": "Vmax_13",
        f"starch_production{c_chl}{tissue}": "Vmax_starch",
    }
    return create_model(
        # Poolman
        partial(add_poolman, c_chl=c_chl, tissue=tissue),
        partial(add_rubisco_carboxylase, c_chl=c_chl, tissue=tissue),
        partial(add_rubisco_oxygenase, c_chl=c_chl, tissue=tissue),
        partial(remove_parameters, parameters=["NADPH", "NADP"]),
        partial(remove_static_atp_production, c_chl=c_chl, tissue=tissue),
        # Matuszynska
        partial(add_matuszynska, c_chl=c_chl, tissue=tissue),
        partial(connect_poolman_matuszynska, c_chl=c_chl, tissue=tissue),
        partial(update_rubisco_carboxylase_dynamic_nadph, c_chl=c_chl, tissue=tissue),
        partial(update_rubisco_oxygenase_dynamic_nadph, c_chl=c_chl, tissue=tissue),
        partial(
            add_thioredoxin_regulation,
            enzyme_vmaxes=enzyme_vmaxes,
            c_chl=c_chl,
            tissue=tissue,
        ),
        partial(add_mehler, c_chl=c_chl, tissue=tissue),
        partial(add_consumption, c_chl=c_chl, tissue=tissue),
        # Yokota
        partial(add_yokota, c_chl=c_chl, c_per=c_per, c_mit=c_mit, tissue=tissue),
        partial(connect_poolman_and_yokota, c_chl=c_chl, c_per=c_per, tissue=tissue),
        partial(
            update_yokota_reactions_dynamic_nadph,
            c_chl=c_chl,
            c_per=c_per,
            c_mit=c_mit,
            tissue=tissue,
        ),
    )


def get_c3_mesophyll(
    c_chl: str = "",
    c_per: str = "",
    c_mit: str = "",
    tissue: str = "",
) -> Model:
    model = get_pmy(c_chl=c_chl, c_per=c_per, c_mit=c_mit, tissue=tissue)
    return modify_model(
        model,
        # Dynamic CO2
        partial(add_dynamic_co2, c_chl=c_chl, tissue=tissue),
        partial(update_rubisco_carboxylase_dynamic_co2, c_chl=c_chl, tissue=tissue),
        partial(update_rubisco_oxygenase_dynamic_co2, c_chl=c_chl, tissue=tissue),
        partial(
            update_glycine_decarboxylase_dynamic_co2,
            c_chl=c_chl,
            c_per=c_per,
            c_mit=c_mit,
            tissue=tissue,
        ),
    )


def get_c3_bundle_sheath(c_chl: str = "", tissue: str = "") -> Model:
    """Assumption: No oxygen in the bundle sheath, thus no photorespiration"""
    model = get_poolman_matuszynska(c_chl=c_chl, tissue=tissue)
    return modify_model(
        model,
        # Dynamic CO2
        partial(add_dynamic_co2, c_chl=c_chl, tissue=tissue),
        partial(update_rubisco_carboxylase_dynamic_co2, c_chl=c_chl, tissue=tissue),
    )


def get_two_compartment_model(
    c_chl: str = "",
    c_per: str = "",
    c_mit: str = "",
    tissue1: str = "_meso",
    tissue2: str = "_bs",
) -> Model:
    m1 = get_c3_mesophyll(c_chl=c_chl, c_per=c_per, c_mit=c_mit, tissue=tissue1)
    m2 = get_c3_bundle_sheath(c_chl=c_chl, tissue=tissue2)
    return m1 + m2


def get_gdc_reroute(
    c_chl: str = "",
    c_per: str = "",
    c_mit: str = "",
    tissue1: str = "_meso",
    tissue2: str = "_bs",
) -> Model:
    model = get_two_compartment_model(
        c_chl=c_chl,
        c_per=c_per,
        c_mit=c_mit,
        tissue1=tissue1,
        tissue2=tissue2,
    )
    return modify_model(
        model,
        partial(
            add_gcd_reroute,
            c_chl=c_chl,
            c_per=c_per,
            c_mit=c_mit,
            tissue1="_meso",
            tissue2="_bs",
        ),
    )


def get_taco_shunt(
    c_chl: str = "",
    c_per: str = "",
    c_mit: str = "",
    tissue1: str = "_meso",
    tissue2: str = "_bs",
) -> Model:
    model = get_two_compartment_model(
        c_chl=c_chl,
        c_per=c_per,
        c_mit=c_mit,
        tissue1=tissue1,
        tissue2=tissue2,
    )
    return modify_model(
        model,
        partial(add_taco_bypass_imm, c_chl=c_chl, tissue=tissue1),
        partial(
            multiply_parameter,
            parameter="vmax_glycine_decarboxylase",
            factor=0.1,
        ),
    )


def get_thf_bypass(
    c_chl: str = "",
    c_per: str = "",
    c_mit: str = "",
    tissue1: str = "_meso",
    tissue2: str = "_bs",
) -> Model:
    model = get_two_compartment_model(
        c_chl=c_chl,
        c_per=c_per,
        c_mit=c_mit,
        tissue1=tissue1,
        tissue2=tissue2,
    )
    return modify_model(
        model,
        partial(
            add_thf_bypass_rma,
            c_chl=c_chl,
            c_per=c_per,
            c_mit=c_mit,
            tissue=tissue1,
        ),
        partial(
            multiply_parameter,
            parameter="vmax_glycine_decarboxylase",
            factor=0.1,
        ),
    )


def get_hydroxyaspartate_shunt(
    c_chl: str = "",
    c_per: str = "",
    c_mit: str = "",
    tissue1: str = "_meso",
    tissue2: str = "_bs",
) -> Model:
    model = get_two_compartment_model(
        c_chl=c_chl,
        c_per=c_per,
        c_mit=c_mit,
        tissue1=tissue1,
        tissue2=tissue2,
    )
    return modify_model(
        model,
        # partial(add_hydroxyasparate_shunt_ima, tissue1="_meso", tissue2="_bs"),
        partial(
            add_hydroxyasparate_shunt_rma,
            c_chl=c_chl,
            c_per=c_per,
            tissue1=tissue1,
            tissue2=tissue2,
        ),
        partial(
            multiply_parameter,
            parameter="vmax_glycine_decarboxylase",
            factor=0.1,
        ),
    )
