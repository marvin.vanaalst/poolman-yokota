from .shared_algebraic_functions import moiety_1
from .shared_rate_functions import mass_action_1s, mass_action_2s, proportional
from .utils import filter_element, rename_element
from modelbase.ode import Model
from typing import List


def add_thioredoxin_regulation(
    model: Model, enzyme_vmaxes: dict[str, str], c_chl: str, tissue: str
) -> Model:
    """Add thioredoxin regulation of the CBB cycle"""

    model.add_parameters(
        {
            "thioredoxin_tot": 1,
            "e_cbb_tot": 6,
            "k_fd_tr_reductase": 1,
            "k_e_cbb_activation": 1,
            "k_e_cbb_relaxation": 0.1,
        }
    )
    model.add_compounds(
        [
            f"TR_ox{c_chl}{tissue}",
            f"E_inactive{c_chl}{tissue}",
        ]
    )

    model.add_algebraic_module(
        module_name=f"thioredoxin_moiety{c_chl}{tissue}",
        function=moiety_1,
        compounds=[f"TR_ox{c_chl}{tissue}"],
        derived_compounds=[f"TR_red{c_chl}{tissue}"],
        parameters=["thioredoxin_tot"],
    )
    model.add_algebraic_module(
        module_name=f"e_cbb{c_chl}{tissue}",
        function=moiety_1,
        compounds=[f"E_inactive{c_chl}{tissue}"],
        derived_compounds=[f"E_active{c_chl}{tissue}"],
        parameters=["e_cbb_tot"],
    )

    for rate_name, old_par in enzyme_vmaxes.items():
        new_par = f"{old_par}_app{tissue}"
        model.add_algebraic_module(
            module_name=new_par,
            function=proportional,
            derived_compounds=[new_par],
            compounds=[f"E_active{c_chl}{tissue}"],
            parameters=[old_par],
        )
        rate = model.rates[rate_name]
        model.update_rate(
            rate_name=rate_name,
            substrates=rate.substrates,
            products=rate.products,
            modifiers=rate.modifiers + [new_par],
            parameters=filter_element(rate.parameters, old_par),
            dynamic_variables=rate.dynamic_variables + [new_par],
            args=rename_element(rate.args, old_par, new_par),
            reversible=rate.reversible,
        )

    model.add_reaction(
        rate_name=f"vFdTrReductase{c_chl}{tissue}",
        function=mass_action_2s,
        stoichiometry={
            f"TR_ox{c_chl}{tissue}": -1,
            f"Fd{c_chl}{tissue}": 1,
        },
        modifiers=[f"Fdred{c_chl}{tissue}"],
        parameters=["k_fd_tr_reductase"],
    )
    model.add_reaction(
        rate_name=f"vE_activation{c_chl}{tissue}",
        function=mass_action_2s,
        stoichiometry={
            f"E_inactive{c_chl}{tissue}": -5,
            f"TR_ox{c_chl}{tissue}": 5,
        },
        modifiers=[f"TR_red{c_chl}{tissue}"],
        parameters=["k_e_cbb_activation"],
    )
    model.add_reaction(
        rate_name=f"vE_inactivation{c_chl}{tissue}",
        function=mass_action_1s,
        stoichiometry={f"E_inactive{c_chl}{tissue}": 5},
        modifiers=[f"E_active{c_chl}{tissue}"],
        parameters=["k_e_cbb_relaxation"],
    )
    return model
